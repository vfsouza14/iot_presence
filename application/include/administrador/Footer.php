<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white">Presence IOT <?php echo date("Y"); ?></span>
        </div>

        <div class="col-xs-6 col-sm-6 text-right hidden-xs">
            <div class="txt-color-white inline-block">
                <!--<i class="txt-color-blueLight hidden-mobile">Última atividade da conta: <i class="fa fa-clock-o"></i> <strong>52 minutos &nbsp;</strong> </i>-->
            </div>
            <!-- end div-->
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>