<header id="header" style="height: 50px;">
    <div id="logo-group">



        <?php //include_once APPLICATION_PATH . '/include/administrador/Notify.php'; ?>
    </div>

    <!-- #PROJECTS: projects dropdown -->
    <?php //include_once APPLICATION_PATH . '/include/administrador/Projects.php'; ?>
    <!-- end projects dropdown -->

    <!-- #TOGGLE LAYOUT BUTTONS -->
    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <?php //include_once APPLICATION_PATH . '/include/mobile/MenuTop.php'; ?>

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo $this->baseUrl(); ?>/administrador/conta/logout" title="Sair" data-action="userLogout" data-logout-msg="Você pode melhorar sua segurança ainda mais depois de sair fechando este navegador aberto"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->

        <!-- #SEARCH -->
        <!-- input: search field -->
        <?php //include_once APPLICATION_PATH . '/include/administrador/Search.php'; ?>
        <!-- end input: search field -->

        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>

        <div id="troca-senha" class="btn-header transparent pull-right">
            <span> <a onclick="return modalLoad(this.href);" href="/administrador/conta/trocar-senha-logado/" title="Trocar senha"><i class="fa fa-key"></i></a> </span>
        </div>

        <!-- end fullscreen button -->

        <!-- #Voice Command: Start Speech -->
        <?php //include_once APPLICATION_PATH . '/include/administrador/VoiceCommand.php'; ?>
        <!-- end voice command -->

        <!-- multiple lang dropdown : find all flags in the flags page -->
        <?php //include_once APPLICATION_PATH . '/include/administrador/Language.php'; ?>
        <!-- end multiple lang -->

    </div>
    <!-- end pulled right: nav area -->

</header>