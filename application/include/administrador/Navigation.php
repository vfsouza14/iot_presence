<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 

            <!--<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut"> -->
            <a href="javascript:void(0);" id="show-shortcut">
                <img src="<?php echo AppUtil::getFileView(Zend_Auth::getInstance()->getIdentity(), UploadUtil::UPLOAD_PATH_USUARIO, '160') ?>" alt="me" class="online" /> 
                <span>
                    <?php echo Zend_Auth::getInstance()->getIdentity()->administrador_nome ?>
                    <?php echo Zend_Auth::getInstance()->getIdentity()->aluno_nome ?>
                    <?php echo Zend_Auth::getInstance()->getIdentity()->professor_nome ?>
                </span>
                <!--<i class="fa fa-angle-down"></i>-->
            </a> 

        </span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
    <nav>
        <!-- 
        NOTE: Notice the gaps after each icon usage <i></i>..
        Please note that these links work a bit different than
        traditional href="" links. See documentation for details.
        -->

        <ul>

            <li>
                <a href="/administrador/dashboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
            </li>
            <?php if (Empresa::isMaster()) : ?>
                <li>
                    <a href="/administrador/empresa" title="Empresas"><i class="fa fa-lg fa-fw fa-building-o"></i> <span class="menu-item-parent">Empresas</span></a>
                </li>
                <li>
                    <a href="/administrador/conta-bancaria" title="Contas Bancárias"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Contas Bancárias</span></a>
                </li>
            <?php endif; ?>
            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">Site</span></a>
                <ul>
                    <li>
                        <a href="/administrador/empresa/configurar" title="Configurações"><i class="fa fa-lg fa-fw fa-gear"></i> Configurações</a>
                    </li>
                    <li>
                        <a href="/administrador/banner"><i class="fa fa-lg fa-fw fa-external-link"></i> Banners</a>
                    </li>
                    <li>
                        <a href="/administrador/aluno-comunicado"><i class="fa fa-lg fa-fw fa-comment"></i> Aluno Comunicados</a>
                    </li>                    
                    <li>
                        <a href="/administrador/pagina"><i class="fa fa-lg fa-fw fa-sitemap"></i> Páginas</a>
                    </li>
                    <li>
                        <a href="/administrador/recado"><i class="fa fa-lg fa-fw fa-comment"></i> Mural de Recados</a>
                    </li>
                    <li>
                        <a href="/administrador/noticia"><i class="fa fa-lg fa-fw fa-rss"></i> Notícias</a>
                    </li>
                    <!--<li>
                        <a href="/administrador/foto"><i class="fa fa-lg fa-fw fa-camera"></i> Galeria de Fotos</a>
                    </li>-->

                </ul>
            </li>

            <li>
                <a href="/administrador/administrador" title="Administradores"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Administradores</span></a>
                <?php if (Empresa::isMaster()) : ?>
                    <ul>
                        <li>
                            <a href="/administrador/administrador" title="Usuários">Usuários</a>
                        </li>
                        <li>
                            <a href="/administrador/usuario-funcionalidade" title="Funcionalidades">Funcionalidades</a>
                        </li>
                    </ul>
                <?php endif; ?>
            </li>
            <li>
                <a href="/administrador/agenciador" title="Agenciadores"><i class="fa fa-lg fa-fw fa-briefcase"></i> <span class="menu-item-parent">Agenciadores</span></a>
            </li>
            <li>
                <a href="/administrador/turma" title="Turmas"><i class="fa fa-lg fa-fw fa-bookmark"></i> <span class="menu-item-parent">Turmas</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-graduation-cap"></i> <span class="menu-item-parent">Alunos</span></a>
                <ul>

                    <li>
                        <a href="/administrador/aluno" title="Lista de Alunos">Lista de Alunos</a>
                    </li>

                    <li>
                        <a href="/administrador/aluno-inadimplente" title="Inadimplentes">Inadimplentes</a>
                    </li>
                    <!--
                    <li>
                        <a href="/administrador/aluno-negociacao" title="Negociação">Negociação</a>
                    </li>
                    <li>
                        <a href="/administrador/aluno-renegociacao" title="2º Negociação">2º Negociação</a>
                    </li>
                    -->
                    <li>
                        <a href="/administrador/aluno-tcc" title="Lista TCC">Lista TCC</a>
                    </li>
                    <li>
                        <a href="/administrador/aluno-estagio" title="Lista TCC">Lista Estágio Supervionado</a>
                    </li>                    
                    <li>
                        <a href="/administrador/aluno-carteira" title="Solicitação de Carteira de Estudante">Carteira de Estudante</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-book"></i> <span class="menu-item-parent">Cursos</span></a>
                <ul>

                    <li>
                        <a href="/administrador/curso" title="Lista de Cursos">Lista de Cursos</a>
                    </li>
                    <li>
                        <a href="/administrador/curso-tipo" title="Tipos de Cursos">Tipos de Cursos</a>
                    </li>
                    <li>
                        <a href="/administrador/curso-area" title="Área de Cursos">Área de Cursos</a>
                    </li>
                    <li>
                        <a href="/administrador/modulo-comum" title="Modulo Básico">Modulo Básico</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="/administrador/professor" title="Professores"><i class="fa fa-lg fa-fw fa-star"></i> <span class="menu-item-parent">Professores</span></a>
            </li>

            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Financeiro</span></a>
                <ul>
                    <li>
                        <a href="/administrador/boleto" title="Boletos">Boletos</a>
                    </li>
                    <li>
                        <a href="/administrador/lancamento/contas-pagar" title="Contas a Pagar">Contas a Pagar</a>
                    </li>
                    <li>
                        <a href="/administrador/lancamento/contas-receber" title="Contas a Receber">Contas a Receber</a>
                    </li> 
                    <li>
                        <a href="/administrador/plano-de-contas/" title="Tipos de Conta">Tipos de Conta</a>
                    </li>
                </ul>
            </li>

            <!--
            <li>
                <a href="/administrador/secretaria" title="Secretaria"><i class="fa fa-lg fa-fw fa-asterisk"></i> <span class="menu-item-parent">Secretaria</span></a>
            </li>
            -->

            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-envelope-o"></i> <span class="menu-item-parent">Newsletter</span></a>
                <ul>
                    <li>
                        <a href="/administrador/comunicado" title="Comunicados">Comunicados</a>
                    </li>
                    <li>
                        <a href="/administrador/comunicado/newsletter-contato" title="E-mails Cadastrados">E-mails Cadastrados</a>
                    </li>

                </ul>
            </li>



            <?php if (Empresa::isMaster()) : ?>
                <li>
                    <a href="/smart/dashboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
                </li>
                <li>
                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/inbox.html"><i class="fa fa-lg fa-fw fa-inbox"></i> <span class="menu-item-parent">Inbox</span><span class="badge pull-right inbox-badge">14</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Graphs</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/flot.html">Flot Chart</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/morris.html">Morris Charts</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/inline-charts.html">Inline Charts</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/dygraphs.html">Dygraphs <span class="badge pull-right inbox-badge bg-color-yellow">new</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Tables</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/table.html">Normal Tables</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/datatables.html">Data Tables <span class="badge inbox-badge bg-color-greenLight">v1.10</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/jqgrid.html">Jquery Grid</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Forms</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/form-elements.html">Smart Form Elements</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/form-templates.html">Smart Form Layouts</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/validation.html">Smart Form Validation</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/bootstrap-forms.html">Bootstrap Form Elements</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/plugins.html">Form Plugins</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/wizard.html">Wizards</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/other-editors.html">Bootstrap Editors</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/dropzone.html">Dropzone</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/image-editor.html">Image Cropping <span class="badge pull-right inbox-badge bg-color-yellow">new</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">UI Elements</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/general-elements.html">General Elements</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/buttons.html">Buttons</a>
                        </li>
                        <li>
                            <a href="#">Icons</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/fa.html"><i class="fa fa-plane"></i> Font Awesome</a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/glyph.html"><i class="glyphicon glyphicon-plane"></i> Glyph Icons</a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/flags.html"><i class="fa fa-flag"></i> Flags</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/grid.html">Grid</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/treeview.html">Tree View</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/nestable-list.html">Nestable Lists</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/jqui.html">JQuery UI</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/typography.html">Typography</a>
                        </li>
                        <li>
                            <a href="#">Six Level Menu</a>
                            <ul>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-folder-open"></i> Item #2</a>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-fw fa-folder-open"></i> Sub #2.1 </a>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-fw fa-file-text"></i> Item #2.1.1</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-fw fa-plus"></i> Expand</a>
                                                    <ul>
                                                        <li>
                                                            <a href="#"><i class="fa fa-fw fa-file-text"></i> File</a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><i class="fa fa-fw fa-trash-o"></i> Delete</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-folder-open"></i> Item #3</a>

                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-fw fa-folder-open"></i> 3ed Level </a>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-fw fa-file-text"></i> File</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-fw fa-file-text"></i> File</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>

                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/calendar.html"><i class="fa fa-lg fa-fw fa-calendar"><em>3</em></i> <span class="menu-item-parent">Calendar</span></a>
                </li>
                <li>
                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/widgets.html"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Widgets</span></a>
                </li>
                <li>
                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/gallery.html"><i class="fa fa-lg fa-fw fa-picture-o"></i> <span class="menu-item-parent">Gallery</span></a>
                </li>
                <li>
                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/gmap-xml.html"><i class="fa fa-lg fa-fw fa-map-marker"></i> <span class="menu-item-parent">GMap Skins</span><span class="badge bg-color-greenLight pull-right inbox-badge">9</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-windows"></i> <span class="menu-item-parent">Miscellaneous</span></a>
                    <ul>
                        <li>
                            <a href="#"><i class="fa fa-file"></i> Other Pages</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/forum.html">Forum Layout</a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/profile.html">Profile</a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/timeline.html">Timeline</a>
                                </li>
                            </ul>	
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/pricing-table.html">Pricing Tables</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/invoice.html">Invoice</a>
                        </li>
                        <li>
                            <a href="login.html" target="_top">Login</a>
                        </li>
                        <li>
                            <a href="register.html" target="_top">Register</a>
                        </li>
                        <li>
                            <a href="lock.html" target="_top">Locked Screen</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/error404.html">Error 404</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/error500.html">Error 500</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/blank_.html">Blank Page</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/email-template.html">Email Template</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/search.html">Search Page</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/ckeditor.html">CK Editor</a>
                        </li>
                    </ul>
                </li>
                <li class="top-menu-hidden">
                    <a href="#"><i class="fa fa-lg fa-fw fa-cube txt-color-blue"></i> <span class="menu-item-parent">SmartAdmin Intel</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/difver.html"><i class="fa fa-stack-overflow"></i> Different Versions</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->baseUrl(); ?>/content/smart/ajax/applayout.html"><i class="fa fa-cube"></i> App Settings</a>
                        </li>
                        <li>
                            <a href="http://bootstraphunter.com/smartadmin/BUGTRACK/track_/documentation/index.html" target="_blank"><i class="fa fa-book"></i> Documentation</a>
                        </li>
                        <li>
                            <a href="http://bootstraphunter.com/smartadmin/BUGTRACK/track_/" target="_blank"><i class="fa fa-bug"></i> Bug Tracker</a>
                        </li>
                    </ul>
                </li>

            <?php endif; ?>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>