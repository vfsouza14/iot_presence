<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 

            <!--<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut"> -->
            <a href="javascript:void(0);" id="show-shortcut">
                <img src="<?php echo AppUtil::getFileView(Zend_Auth::getInstance()->getIdentity(), UploadUtil::UPLOAD_PATH_USUARIO, '160') ?>" alt="me" class="online" /> 
                <span>
                    <?php echo Zend_Auth::getInstance()->getIdentity()->administrador_nome ?>
                    <?php echo Zend_Auth::getInstance()->getIdentity()->aluno_nome ?>
                    <?php echo Zend_Auth::getInstance()->getIdentity()->professor_nome ?>
                </span>
                <!--<i class="fa fa-angle-down"></i>-->
            </a> 

        </span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
    <nav>
        <!-- 
        NOTE: Notice the gaps after each icon usage <i></i>..
        Please note that these links work a bit different than
        traditional href="" links. See documentation for details.
        -->

        <ul>

            <li>
                <a href="/administrador/dashboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
            </li>
            <?php if (Empresa::isMaster()) : ?>
                <li>
                    <a href="/administrador/empresa" title="Empresas"><i class="fa fa-lg fa-fw fa-building-o"></i> <span class="menu-item-parent">Empresas</span></a>
                </li>
                <li>
                    <a href="/administrador/conta-bancaria" title="Contas Bancárias"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Contas Bancárias</span></a>
                </li>
            <?php endif; ?>
            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">Site</span></a>
                <ul>
                    <li>
                        <a href="/administrador/aluno-comunicado"><i class="fa fa-lg fa-fw fa-bullhorn"></i> Aluno Comunicados</a>
                    </li>                     
                    <li>
                        <a href="/administrador/recado"><i class="fa fa-lg fa-fw fa-comment"></i> Mural de Recados</a>
                    </li>
                    <li>
                        <a href="/administrador/noticia"><i class="fa fa-lg fa-fw fa-rss"></i> Notícias</a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="/administrador/agenciador" title="Agenciadores"><i class="fa fa-lg fa-fw fa-briefcase"></i> <span class="menu-item-parent">Agenciadores</span></a>
            </li>

            <li>
                <a href="#" title="Turmas"><i class="fa fa-lg fa-fw fa-bookmark"></i> <span class="menu-item-parent">Turmas</span></a>
                <ul>
                    <li>
                        <a href="/administrador/turma" title="Turmas">Lista de Turmas</a>
                    </li>
                    <li>
                        <a href="/administrador/turma/agenda" title="Agenda Educacional">Agenda Educacional</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-graduation-cap"></i> <span class="menu-item-parent">Alunos</span></a>
                <ul>

                    <li>
                        <a href="/administrador/aluno" title="Lista de Alunos">Lista de Alunos</a>
                    </li>
                    <li>
                        <a href="/administrador/aluno-tcc" title="Lista TCC">Lista TCC</a>
                    </li>
                    <li>
                        <a href="/administrador/aluno-carteira" title="Solicitação de Carteira de Estudante">Carteira de Estudante</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-lg fa-fw fa-book"></i> <span class="menu-item-parent">Cursos</span></a>
                <ul>

                    <li>
                        <a href="/administrador/curso" title="Lista de Cursos">Lista de Cursos</a>
                    </li>
                </ul>
            </li>

        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>