<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is -->
            <!--<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut"> -->
                <a href="javascript:void(0);" id="show-shortcut">
                    <span>
                        <?php echo Zend_Auth::getInstance()->getIdentity()->email ?>
                    </span>
                    <!--<i class="fa fa-angle-down"></i>-->
                </a>
            </span>
        </div>
        <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
-->
<nav>
        <!--
        NOTE: Notice the gaps after each icon usage <i></i>..
        Please note that these links work a bit different than
        traditional href="" links. See documentation for details.
    -->

    <ul>
        <li>
            <a href="/administrador/dashboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
        </li>

        <li>
            <a href="/administrador/aluno"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Alunos</span></a>
        </li>
        <li>
            <a href="/administrador/turma"><i class="fa fa-lg fa-fw fa-certificate"></i> <span class="menu-item-parent">Turma</span></a>
        </li>
        <li>
            <a href="/administrador/professor"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Professores</span></a>
        </li>

    </ul>
</nav>
<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>