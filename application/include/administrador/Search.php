<form action="#ajax/search.html" class="header-search pull-right">
    <input id="search-fld" type="text" name="param" placeholder="Find reports and more">
    <button type="submit">
        <i class="fa fa-search"></i>
    </button>
    <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
</form>