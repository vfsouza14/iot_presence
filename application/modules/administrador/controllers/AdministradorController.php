<?php

class Administrador_AdministradorController extends Zend_Controller_Action {

    private $_administradorRepository;
    private $_empresaRepository;
    private $_usuarioFuncionalidadeRepository;
    private $_usuarioPerfilRepository;
    private $_equipeRepository;
    private $_equipeUsuarioRepository;

    public function init() {
        $this->_administradorRepository = new AdministradorRepository();
        $this->_empresaRepository = new EmpresaRepository();
        $this->_usuarioFuncionalidadeRepository = new UsuarioGrupoPermissaoFuncionalidadeRepository();
        $this->_usuarioPerfilRepository = new UsuarioPerfilRepository();
        $this->_equipeRepository = new EquipeRepository();
        $this->_equipeUsuarioRepository = new EquipeUsuarioRepository();
    }

    public function indexAction() {

        if (!$this->getRequest()->isPost()) {
            $this->getRequest()->setParams(RepositoryFilter::getFilterSession());
            $params = $this->getRequest()->getParams();
        } else {
            $params = $this->getRequest()->getPost();
        }

        if ($params['status'] == "" || $params['status'] == 0)
            $params['status'] = DaoGeneric::ATIVO;

        $filter = new RepositoryFilter($params);
        $filter->addJoinFilter('p.Usuario u');
        $filter->addTextFilter('p.nome', $params['nome']);
        $filter->addTextFilter('u.email', $params['email']);
        $filter->addFilter('status = ?', $params['status']);
        $filter->addSelectFilter('u.usuario_perfil_id', $params['perfil']);

        if (!Empresa::isMaster())
            $filter->addFilter('p.empresa_id = ?', Empresa::getLogged()->id);

        if ($this->getRequest()->isPost()) {
            $filter->setFilterSession();
        }

        $sortParam = ($params["sort"]) ? $params["sort"] : 'p.nome';
        $orderParam = ($params["order"]) ? $params["order"] : 'DESC';
        $orderby = new RepositoryOrder($params);
        $orderby->addOrder($sortParam, ($orderParam == 'ASC') ? 'DESC' : 'ASC');

        $list = new Zend_Paginator(new My_Zend_Paginator_Adapter_Doctrine($this->_administradorRepository->getListByFilter($filter, $orderby)));
        $list->setItemCountPerPage(20);
        $list->setCurrentPageNumber($params["page"]);
        $this->view->list = $list;
        $this->view->list_params = array('filter' => $filter);

        $this->view->repository_filter = $filter;
        $this->view->repository_order = $orderby;

        $this->view->perfis = $this->_usuarioPerfilRepository->getListByGrupo(Usuario::GRUPO_ADMINISTRADOR);
    }

    public function novoAction() {
        $administrador = new Administrador();

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $this->_setData($administrador, $data);

            $validate = $this->_validate($administrador);
            if (!isset($validate) || $validate == "") {
                $administrador->save();
                UploadUtil::uploadFile($administrador->Usuario, $_FILES['file'], UploadUtil::UPLOAD_PATH_USUARIO);

                $this->_gerarPermissaoUsuario($administrador->usuario_id);

                $this->_helper->FlashMessenger('Administrador cadastrado com sucesso.');
                $this->_redirect($this->view->baseUrl() . '/administrador/administrador');
            } else {
                $this->_helper->FlashMessenger(array('warning' => $validate));
            }
        }

        $this->view->empresas = $this->_empresaRepository->getList();
        $this->view->administrador = $administrador;
        $this->view->funcionalidades = $this->_usuarioFuncionalidadeRepository->getListByModule('administrador');
        $this->view->perfis = $this->_usuarioPerfilRepository->getListByGrupo(Usuario::GRUPO_ADMINISTRADOR);
    }

    public function editarAction() {
        $id = $this->getRequest()->getParam('id');
        $administrador = $this->_administradorRepository->getById($id);
        $equipes = $this->_equipeRepository->getList();

        $deletedImagem = $this->_deleteImage($administrador);

        if (!$deletedImagem && $this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $this->_setData($administrador, $data);

            $validate = $this->_validate($administrador);
            if (!isset($validate) || $validate == "") {
                $administrador->save();
                $administrador->Usuario->save();
                $this->_setOrdemEquipe(Equipe::EQUIPE_SECRETARIA);

                UploadUtil::uploadFile($administrador->Usuario, $_FILES['file'], UploadUtil::UPLOAD_PATH_USUARIO);

                $this->_gerarPermissaoUsuario($administrador->usuario_id);

                $this->_helper->FlashMessenger('Administrador alterado com sucesso.');
                $this->_redirect($this->view->baseUrl() . '/administrador/administrador');
            } else {
                $this->_helper->FlashMessenger(array('warning' => $validate));
            }
        }

        $this->view->empresas = $this->_empresaRepository->getList();
        $this->view->administrador = $administrador;
        $this->view->funcionalidades = $this->_usuarioFuncionalidadeRepository->getListByModule('administrador');
        $this->view->equipes = $equipes;
        $this->view->perfis = $this->_usuarioPerfilRepository->getListByGrupo(Usuario::GRUPO_ADMINISTRADOR);
    }

    public function deletarAction() {
        $id = $this->getRequest()->getParam('id');

        $administrador = $this->_administradorRepository->getById($id);
        if ($administrador != FALSE) {
            $administrador->delete();
            $administrador->Usuario->UsuarioPermissao->delete();
            $administrador->Usuario->delete();

            UploadUtil::removeFile($administrador->Usuario, UploadUtil::UPLOAD_PATH_USUARIO);
            unlink(TENANT_PERMISSAO_PATH . '/' . $administrador->Usuario->id . ".xml");

            $this->_helper->FlashMessenger('Administrador removido com sucesso.');
        } else {
            $this->_helper->FlashMessenger(array('warning' => 'Atenção você está tentando excluir um administrador que não existe.'));
        }

        $this->_redirect($this->view->baseUrl() . '/administrador/administrador');
    }

    public function _deleteImage($administrador) {
        $result = FALSE;

        if (!$this->getRequest()->getParam('remove')) {
            $result = FALSE;
        } else {
            $result = UploadUtil::removeFile($administrador->Usuario, UploadUtil::UPLOAD_PATH_USUARIO);
            $this->_helper->FlashMessenger(array('warning' => 'Imagem removida com sucesso.'));
        }

        return $result;
    }

    private function _setData(Administrador $administrador, $data) {
        $administrador->nome = AppUtil::setFirstUpWord($data['nome']);
        $administrador->setHorarioAcesso($data['horario_acesso']);
        $administrador->setEmpresa($data['empresa']);

        $this->_setDataUsuario($administrador, $data);
    }

    private function _setDataUsuario(Administrador $administrador, $data) {
        $usuario = new Usuario();

        if ($administrador->Usuario->id > 0) {
            $usuario = $administrador->Usuario;
        }

        $usuario->usuario_grupo_id = Usuario::GRUPO_ADMINISTRADOR;
        $usuario->usuario_perfil_id = $data['perfil'];

        $usuario->email = AppUtil::setWordLower($data['email']);
        $usuario->username = AppUtil::setWordLower($data['username']);

        if (trim($data['senha']) != ""){
            $usuario->senha = trim($data['senha']);
            $usuario->hash = password_hash(trim($data['senha']), PASSWORD_DEFAULT);
        }


        $usuario->status = $data['status'];
        $usuario->setEmpresa($data['empresa']);

        $this->_setDataPermissoes($usuario, $data);
        $this->_setDataEquipe($usuario, $data);

        $administrador->Usuario = $usuario;
    }

    private function _setDataPermissoes(Usuario $usuario, $data) {
        $usuario->UsuarioPermissao->clear();

        foreach ($data['funcionalidade'] as $funcionalidade) {
            $usuarioPermissao = new UsuarioPermissao();
            $usuarioPermissao->usuario_id = $usuario->id;
            $usuarioPermissao->usuario_grupo_permissao_funcionalidade_id = $funcionalidade;
            $usuarioPermissao->permitido = TRUE;
            $usuarioPermissao->setEmpresa($data['empresa']);
            $usuario->UsuarioPermissao->add($usuarioPermissao);
        }
    }

    private function _setDataEquipe(Usuario $usuario, $data){
        $usuario->EquipeUsuario->clear();

        foreach ($data['equipes'] as $equipe){
            $equipe_usuario = new EquipeUsuario();
            $equipe_usuario->equipe_id = $equipe;
            $equipe_usuario->usuario_id = $usuario->id;
            $usuario->EquipeUsuario->add($equipe_usuario);
        }
    }

    private function _setOrdemEquipe($equipe){
        $ordem_equipe = $this->_equipeUsuarioRepository->getEquipe($equipe);

        $count_ordem = 1;
        foreach ($ordem_equipe as $ordem){
            $ordem->fila_ordem = $count_ordem;
            $ordem_equipe->save();
            $count_ordem++;
        }
    }

    private function _validate(Administrador $administrador) {
        $result = '';
        if ($administrador->nome == "")
            $result .= "<li>O campo <b>Nome</b> deve ser informado.</li>";

        if ($administrador->Usuario->usuario_perfil_id <= 0)
            $result .= "<li>O campo <b>Perfil de Acesso</b> deve ser informado.</li>";


        $result .= $this->_validateEmail($administrador);

        if ($administrador->id <= 0 && $administrador->Usuario->senha == "")
            $result .= "<li>O campo <b>Senha</b> deve ser informado.</li>";

        if ($administrador->empresa_id <= 0 || $administrador->Usuario->empresa_id <= 0)
            $result .= "<li>O campo <b>Empresa</b> deve ser informado.</li>";

        return $result;
    }

    private function _validateEmail(Administrador $administrador) {
        $result = '';
        if ($administrador->Usuario->email == "") {
            $result .= "<li>O campo <b>Email</b> deve ser informado.</li>";
        } else if ($this->_administradorRepository->isExiste($administrador)) {
            $result .= "<li>O <b>Email</b> informado já esta registrado.</li>";
        }

        return $result;
    }

    private function _gerarPermissaoEmpresa($empresa) {
        $usuarioRepository = new UsuarioRepository();
        $usuarios = $usuarioRepository->getListByEmpresa($empresa);

        foreach ($usuarios as $usuario) {
            $this->_gerarPermissaoUsuario($usuario->id);
        }
    }

    private function _gerarPermissaoUsuario($usuario_id) {
        $dom = new DOMDocument('1.0', 'ISO-8859-1');

        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->normalizeDocument();

        $xml_funcionalidades = $dom->createElement('funcionalidades');
        $dom->appendChild($xml_funcionalidades);

        $usuarioPermissaoRepository = new UsuarioPermissaoRepository();
        $permissoes = $usuarioPermissaoRepository->getListByUsuario($usuario_id);

        if ($permissoes->count() > 0) {
            $permissao = new UsuarioPermissao();
            foreach ($permissoes as $permissao) {
                $element = $dom->createElement('funcionalidade');
                $element->setAttribute("module", $permissao->UsuarioGrupoPermissaoFuncionalidade->module);
                $element->setAttribute("controller", $permissao->UsuarioGrupoPermissaoFuncionalidade->controller);
                $element->setAttribute("action", $permissao->UsuarioGrupoPermissaoFuncionalidade->action);
                $element->appendChild($dom->createElement('permitido', $permissao->permitido));
                $xml_funcionalidades->appendChild($element);
            }

            header("Content-type: text/xml;");
            $strxml = $dom->saveXML();
            $handle = fopen(TENANT_PERMISSAO_PATH . '/' . $usuario_id . ".xml", "w");
            fwrite($handle, $strxml);
            fclose($handle);
        } else {
            unlink(TENANT_PERMISSAO_PATH . '/' . $usuario_id . ".xml");
        }
    }

}
