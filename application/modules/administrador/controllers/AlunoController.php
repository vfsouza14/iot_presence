<?php

class Administrador_AlunoController extends Zend_Controller_Action {

    private $_alunoRepository;
    private $_matriculaRepository;

    public function init() {
        $this->_alunoRepository = new AlunoRepository();
        $this->_matriculaRepository = new MatriculaRepository();
    }

    public function indexAction() {

        if (!$this->getRequest()->isPost()) {
            $this->getRequest()->setParams(RepositoryFilter::getFilterSession());
            $params = $this->getRequest()->getParams();
        } else {
            $params = $this->getRequest()->getPost();
        }

        $filter = new RepositoryFilter($params);
        $filter->addJoinFilter('p.Aluno a');
        $filter->addJoinFilter('a.Usuario u');
        $filter->addJoinFilter('p.Curso c');
        $filter->addTextFilter('a.nome', $params['nome']);
        $filter->addTextFilter('u.email', $params['email']);
        $filter->addTextFilter('c.descricao', $params['curso']);



        if ($this->getRequest()->isPost()) {
            $filter->setFilterSession();
        }

        $sortParam = ($params["sort"]) ? $params["sort"] : 'a.nome';
        $orderParam = ($params["order"]) ? $params["order"] : 'DESC';
        $orderby = new RepositoryOrder($params);
        $orderby->addOrder($sortParam, ($orderParam == 'ASC') ? 'DESC' : 'ASC');

        $list = new Zend_Paginator(new My_Zend_Paginator_Adapter_Doctrine($this->_matriculaRepository->getListByFilter($filter, $orderby)));
        $list->setItemCountPerPage(20);
        $list->setCurrentPageNumber($params["page"]);
        $this->view->list = $list;
        $this->view->list_params = array('filter' => $filter);

        $this->view->repository_filter = $filter;
        $this->view->repository_order = $orderby;

    }

}
