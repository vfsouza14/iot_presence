<?php

class Administrador_ContaController extends Zend_Controller_Action {

    private $_usuarioRepository;

    public function init() {
        $this->_usuarioRepository = new UsuarioRepository();

    }

    public function indexAction() {

        if ($this->getRequest()->isPost()) {

            $data = $this->getRequest()->getPost();

            $email = $data['email'];
            $senha = $data['password'];

            if (!isset($email) || $email == '' || !isset($senha) || $senha == '') {
                $this->_helper->FlashMessenger(array('error' => 'Os campos para autenticação devem ser preenchidos.'));
                $this->_redirect($this->view->baseUrl() . '/administrador/conta');
            }

            $login = $this->_login($email, $senha);

            if ($login === true) {
                $this->_redirect($this->view->baseUrl() . '/administrador/index/');
            }
        }
    }

    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        SessionUtil::destroyAccessToken();
        $this->_helper->FlashMessenger('Logout efetuado sucesso.');
        $this->_redirect($this->view->baseUrl() . '/administrador/conta');
    }

    public function logoutTrocarSenhaAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        SessionUtil::destroyAccessToken();
        $this->_redirect($this->view->baseUrl() . '/administrador/conta');
    }

    public function lembrarSenhaAction() {
        if ($this->getRequest()->isPost()) {
            $email = $this->getRequest()->getParam('email');
            $administrador = $this->_administradorRepository->getAdministradorByEmail($email);

            if ($administrador == false) {
                $this->_helper->FlashMessenger(array('warning' => 'Desculpe, mas o Email digitado não existe em nosso sistema.'));
            } else {
                $this->_sendMail($administrador);
                $this->_helper->FlashMessenger('Os dados de acesso foram enviados para seu email. Caso não encontre na caixa de entrada, verifique na caixa de spam.');
                $this->_redirect($this->view->baseUrl() . '/administrador/conta');
            }
        }
    }

    public function recuperarSenhaAction() {
        $token = $this->getRequest()->getParam('token');

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $tokenResposta = $this->_decodeTokenAdmin($token);

            $administrador = $this->_administradorRepository->getByUsuarioIdEmail($tokenResposta['usuario_id'], $tokenResposta['email']);

            $validate = $this->_validateRecuperarSenha($administrador, $data);
            if (!isset($validate) || $validate == "") {
                $administrador->Usuario->senha = $data['senha_nova'];
                $administrador->Usuario->hash = password_hash($data['senha_nova'], PASSWORD_DEFAULT);
                $administrador->save();

                $this->_helper->FlashMessenger('Sua senha foi alterada com sucesso.');
                $this->_redirect($this->view->baseUrl() . '/administrador/index/');
            } else {
                $this->_helper->FlashMessenger(array('warning' => $validate));
            }
        }
    }

    public function trocarSenhaAction() {
        $administrador = $this->_administradorRepository->getByUsuario(Usuario::getLogged()->id);

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();

            $validate = $this->_validateSenha($administrador, $data);

            if (!isset($validate) || $validate == "") {
                $administrador->Usuario->senha = $data['senha_nova'];
                $administrador->Usuario->hash = password_hash($data['senha_nova'], PASSWORD_DEFAULT);
                $administrador->save();

                $this->_helper->FlashMessenger('Sua senha foi alterada com sucesso. Faça o login novamente.');
                $this->logoutTrocarSenhaAction();
            } else {
                $this->_helper->FlashMessenger(array('warning' => $validate));
            }
        }
        $this->view->administrador = $administrador;
    }

    public function trocarSenhaLogadoAction() {
        $administrador = $this->_administradorRepository->getByUsuario(Usuario::getLogged()->id);

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();

            $validate = $this->_validateSenha($administrador, $data);

            if (!isset($validate) || $validate == "") {
                $administrador->Usuario->senha = $data['senha_nova'];
                $administrador->Usuario->hash = password_hash($data['senha_nova'], PASSWORD_DEFAULT);
                $administrador->save();

            } else {
                $this->_helper->FlashMessenger(array('warning' => $validate));
            }
        }

        $this->view->administrador = $administrador;
    }

    private function _validateSenha($administrador, $data) {
        $result = "";

        if ($data['senha_atual'] != $administrador->Usuario->senha)
            $result .= "<li>O campo <b>Senha Atual</b> está incorreta.</li>";

        if ($data['senha_atual'] == "")
            $result .= "<li>O campo <b>Senha Atual</b> deve ser informado.</li>";

        if ($data['senha_nova'] == "")
            $result .= "<li>O campo <b>Nova Senha</b> deve ser informado.</li>";

        if ($data['senha_nova'] != $data['confirmar-senha'])
            $result .= "<li>A confirmação de senha está incorreta.</li>";

        if ($data['confirmar-senha'] == "")
            $result .= "<li>O campo <b>Confirme Sua Senha</b> deve ser informado.</li>";

        if (!Administrador::isValidPassword($data['senha_nova']))
            $result .= "<li>A senha deve conter de 8 à 20 caracteres com uma combinação de letras maiúsculas e minúsculas, números e símbolos. (A senha não pode conter espaço e aspas).</li>";

        return $result;

    }

    private function _validateRecuperarSenha($administrador, $data) {

        $result = "";

        if ($data['confirmar-senha'] == "")
            $result .= "<li>O campo <b>Confirme Sua Senha</b> deve ser informado.</li>";

        if ($data['senha_nova'] != $data['confirmar-senha'])
            $result .= "<li>A <b>confirmação de senha</b> está incorreta.</li>";

        if(!Administrador::isValidPassword($data['senha_nova']))
            $result .= "<li>A senha deve conter de 8 à 20 caracteres com uma combinação de letras maiúsculas e minúsculas, números e símbolos. (A senha não pode conter espaço e aspas).</li>";

        return $result;

    }

    private function _login($login, $senha) {
        $db_config = Zend_Registry::getInstance()->get('database');
        $dbAdapter = Zend_Db::factory($db_config->db->adapter, array(
                    'host' => $db_config->db->params->host,
                    'username' => $db_config->db->params->username,
                    'password' => $db_config->db->params->password,
                    'dbname' => $db_config->db->params->dbname,
                    'driver_options' => array(
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                    )
        ));

        $adpter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $adpter->setTableName('usuario')
                ->setIdentityColumn('usuario.email')
                ->setCredentialColumn('usuario.senha');


        $select = $adpter->getDbSelect();

        $select->where('usuario.tipo = "' . Usuario::ADMINISTRADOR . '"');
        $select->where('usuario.status = "' . Usuario::ATIVO . '"');

        $administrador = $this->_usuarioRepository->getByEmail($login);
        $verifica_hash = password_verify($senha, $administrador->hash_senha);

        if($verifica_hash == true){

            $adpter->setIdentity($login)
                ->setCredential($senha);

            $auth = Zend_Auth::getInstance();
            $auth->authenticate($adpter);

            $info = $adpter->getResultRowObject(null, 'senha');
            $storage = $auth->getStorage();
            $storage->write($info);

            $token = AuthUtil::tokenGenerate($info->id, $info->email, $info->usuario_grupo_id);
            SessionUtil::setAccessToken($token->access_token);

        } else {
            $this->_helper->FlashMessenger(array('warning' => 'Usuário e/ou senha inválidos!'));
            $this->_redirect($this->view->baseUrl() . '/administrador/conta');
        }
        return true;
    }

    public function _generateTokenAdmin($administrador)
    {
        return base64_encode(base64_encode($administrador->usuario_id) . "." .  base64_encode($administrador->Usuario->email));
    }

    public function _decodeTokenAdmin($token)
    {
        $token_explodido = explode(".", base64_decode($token));
        $token_descoberto = [
            'usuario_id' => base64_decode($token_explodido[0]),
            'email' => base64_decode($token_explodido[1])
        ];

        return $token_descoberto;
    }

}
