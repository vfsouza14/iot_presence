<?php

class Administrador_IndexController extends Zend_Controller_Action {

    public function init() {
         $this->_helper->layout->setLayout('Administrador');
    }

    public function indexAction() {
       
       if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
            
        } else {
        	$r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        	$r->gotoUrl(Zend_Controller_Front::getInstance()->getBaseUrl() . '/administrador/conta/')->redirectAndExit();	
        }
        
    }

}
