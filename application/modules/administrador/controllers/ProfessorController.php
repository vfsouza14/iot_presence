<?php

class Administrador_ProfessorController extends Zend_Controller_Action {

    private $_professorRepository;

    public function init() {
        $this->_professorRepository = new ProfessorRepository();
    }

    public function indexAction() {

        if (!$this->getRequest()->isPost()) {
            $this->getRequest()->setParams(RepositoryFilter::getFilterSession());
            $params = $this->getRequest()->getParams();
        } else {
            $params = $this->getRequest()->getPost();
        }

        $filter = new RepositoryFilter($params);
        $filter->addJoinFilter('p.Usuario u');
        $filter->addTextFilter('p.nome', $params['nome']);
        $filter->addTextFilter('u.email', $params['email']);


        if ($this->getRequest()->isPost()) {
            $filter->setFilterSession();
        }

        $sortParam = ($params["sort"]) ? $params["sort"] : 'p.nome';
        $orderParam = ($params["order"]) ? $params["order"] : 'DESC';
        $orderby = new RepositoryOrder($params);
        $orderby->addOrder($sortParam, ($orderParam == 'ASC') ? 'DESC' : 'ASC');

        $list = new Zend_Paginator(new My_Zend_Paginator_Adapter_Doctrine($this->_professorRepository->getListByFilter($filter, $orderby)));
        $list->setItemCountPerPage(20);
        $list->setCurrentPageNumber($params["page"]);
        $this->view->list = $list;
        $this->view->list_params = array('filter' => $filter);

        $this->view->repository_filter = $filter;
        $this->view->repository_order = $orderby;

    }

}
