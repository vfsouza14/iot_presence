<?php

class Administrador_TurmaController extends Zend_Controller_Action {

    private $_turmaRepository;

    public function init() {
        $this->_turmaRepository = new TurmaRepository();
    }

    public function indexAction() {

        if (!$this->getRequest()->isPost()) {
            $this->getRequest()->setParams(RepositoryFilter::getFilterSession());
            $params = $this->getRequest()->getParams();
        } else {
            $params = $this->getRequest()->getPost();
        }

        $filter = new RepositoryFilter($params);
        $filter->addJoinFilter('p.Matricula m');
        $filter->addJoinFilter('p.Professor pr');
        $filter->addJoinFilter('m.Aluno a');
        $filter->addJoinFilter('m.Curso c');
        $filter->addTextFilter('a.nome', $params['nome']);
        $filter->addTextFilter('c.descricao', $params['descricao']);
        $filter->addTextFilter('pr.nome', $params['professor_nome']);

        //var_dump($params);die;
        if ($this->getRequest()->isPost()) {
            $filter->setFilterSession();
        }

        $sortParam = ($params["sort"]) ? $params["sort"] : 'a.nome';
        $orderParam = ($params["order"]) ? $params["order"] : 'DESC';
        $orderby = new RepositoryOrder($params);
        $orderby->addOrder($sortParam, ($orderParam == 'ASC') ? 'DESC' : 'ASC');

        $list = new Zend_Paginator(new My_Zend_Paginator_Adapter_Doctrine($this->_turmaRepository->getListByFilter($filter, $orderby)));
        $list->setItemCountPerPage(20);
        $list->setCurrentPageNumber($params["page"]);
        $this->view->list = $list;
        $this->view->list_params = array('filter' => $filter);

        $this->view->repository_filter = $filter;
        $this->view->repository_order = $orderby;

    }

}
