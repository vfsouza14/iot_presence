<?php

/**
 * Regular controller
 * */
class Site_ApiExampleController extends Zend_Controller_Action {


	public function indexAction() {

		/* GET /api/cursos?page[number]=1&page[size]=10&format=json */
		/* GET /api/cursos?page[number]=1&page[size]=10&filter[area]=1,2,3,4&format=json */
		/* GET /api/cursos?page[number]=1&page[size]=10&filter[tipo]=1,2,3,4&format=json */
		/* GET /api/cursos?page[number]=1&page[size]=10&filter[carga_horaria]=660,200&format=json */
		/* GET /api/cursos?page[number]=1&page[size]=10&sort=nome&format=json */
		/* GET /api/cursos?page[number]=1&page[size]=10&sort=nome&filter[nome]=Pedagogia&format=json */

		$data['page[number]'] = '1';
		$data['page[size]'] = '10';
		$data['filter[nome]'] = '';
		$data['filter[area]'] = '1,2,3,4';
		$data['filter[tipo]'] = '1,2,3,4';
		$data['filter[carga_horaria]'] = '660,200';
		$data['sort'] = 'nome';
		$data['format'] = 'json';
		$field_string = http_build_query($data, '', '&');

		$ch = curl_init($this->view->baseUrl() . '/api/cursos');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJleHBpcmF0aW9uX3NlYyI6MzYwMCwiaXNzIjoiZmFtYXJ0LmVkdS5iciIsImlkIjoiMjciLCJuYW1lIjpudWxsLCJjYXRlZ29yeSI6bnVsbCwidXNlciI6InBhdWxvaGVucmlxdWUudGlAZ21haWwuY29tIiwicGFzcyI6ImVjMzVhYmNkIn0.'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);

		$result = curl_exec($ch);

		$result = json_decode($result);

		$this->view->items = $result->items;
	}

	public function cursosTiposAction() {
		
		$ch = curl_init($this->view->baseUrl() . '/api/cursos');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJleHBpcmF0aW9uX3NlYyI6MzYwMCwiaXNzIjoiZmFtYXJ0LmVkdS5iciIsImlkIjoiMjciLCJuYW1lIjpudWxsLCJjYXRlZ29yeSI6bnVsbCwidXNlciI6InBhdWxvLmp1bmlvckBmYWN1bGRhZGVmYW1hcnQuZWR1LmJyIiwicGFzcyI6ImVjMzVhYmNkIn0.'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "TIPOS");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		$result = curl_exec($ch);

		$r = json_decode($result);

		if (curl_errno($ch)) {
			throw new Exception(curl_error($ch));
		}

		curl_close($ch);
		var_dump($r);die;
	}

		/*
		/api/example/generate-key
		*/
		public function generateKeyAction() {
			$data['usuario'] = 'paulo.junior@presence_iot.edu.br';
			$data['senha'] = '******';
			$field_string = http_build_query($data, '', '&');

			$ch = curl_init($this->view->baseUrl() . '/api/auth');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

			$result = curl_exec($ch);

			$r = json_decode($result);

			if (curl_errno($ch)) {
				throw new Exception(curl_error($ch));
			}

			curl_close($ch);
			var_dump($r);die;
		}

	}
