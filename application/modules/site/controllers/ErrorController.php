<?php

class Site_ErrorController extends Zend_Controller_Action {

    public function errorAction() {
        $this->_helper->layout->disableLayout();
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                $this->view->message = 'Page not found';
                //$this->_sendMail($this->view, $errors);
                //$this->_redirect($this->view->baseUrl());
                //$this->_redirect($this->view->baseUrl() . '/site/error/page-not-found');
                // 404 error -- controller or action not found
                //$this->getResponse()->setHttpResponseCode(404);
                break;
            default:

                $this->view->message = 'Application error';
                //$this->_sendMail($this->view, $errors);
		//$this->_redirect($this->view->baseUrl());
                //$this->_redirect($this->view->baseUrl() . '/site/error/application-error');
                // application error
                //$this->getResponse()->setHttpResponseCode(500);
                break;
        }

        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;

        //$this->_sendMail($this->view);
        //$this->_redirect($this->view->baseUrl() . '/admin');
    }
    
    public function error1Action() {
        $this->_helper->layout->disableLayout();
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                $this->view->message = 'Page not found';
                $this->_sendMail($this->view, $errors);
                $this->_redirect($this->view->baseUrl() . '/site/error/page-not-found');
                // 404 error -- controller or action not found
                //$this->getResponse()->setHttpResponseCode(404);
                break;
            default:

                $this->view->message = 'Application error';
                $this->_sendMail($this->view, $errors);
                $this->_redirect($this->view->baseUrl() . '/site/error/application-error');
                // application error
                //$this->getResponse()->setHttpResponseCode(500);
                break;
        }

        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;

        //$this->_sendMail($this->view);
        //$this->_redirect($this->view->baseUrl() . '/admin');
    }

    public function pageNotFoundAction() {
        
    }

    public function applicationErrorAction() {
        
    }

    public function accessDeniedAction() {
        
    }

    private function _sendMail($view, $errors) {
        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;
        
        $usuario = Zend_Auth::getInstance()->getIdentity();
        $empresaRepository = new EmpresaRepository();
        $empresa = $empresaRepository->getById($usuario->empresa_id);

        $app_email = Zend_Registry::getInstance()->get('email');

        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . '/layouts/emails/');
        $html->assign('message', $view->message);
        $html->assign('exception', $view->exception);
        $html->assign('request', $view->request);
        $html->assign('data', AppUtil::convertDateTimeToString(AppUtil::getCurrentDate()));
        $html->assign('escola', $empresa->razao_social);
        $html->assign('usuario', $usuario->nome . " (" . $usuario->email . ")");
        $html->assign('url', $this->view->baseUrl() . $view->request->getRequestUri());
        $body = $html->render('error.phtml');

        $from['name'] = $app_email->mail->from->name;
        $from['email'] = $app_email->mail->from->email;

        $to[] = 'paulojunior18@gmail.com';

        $subject = "[IPB] Falha no Sistema " . AppUtil::getCurrentDate();

        EmailUtil::send($from, $to, $subject, $body);
    }

}
