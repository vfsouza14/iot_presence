<?php
    class Site_ExternalAuthController extends Zend_Controller_Action
    {

        public function indexAction()
        {
            $token = $this->getRequest()->getParam('token');
            $url = $this->getRequest()->getParam('url');

            $token = base64_decode($token);
            $token_arr = explode(".", $token);
            $usuario_id = base64_decode($token_arr[0]);
            $cpf = base64_decode($token_arr[1]);

            $usuarioRepository = new UsuarioRepository();
            $usuario = $usuarioRepository->getById($usuario_id);

            if ($cpf == "" || $usuario->senha == "" || $url == "") {
                $this->_helper->FlashMessenger(array('warning' => 'Token Inválido'));
                $this->_redirect($this->view->baseUrl() . '/aluno/conta');
            }

            $this->_login($cpf, $usuario->senha, $url);


        }

        private function _login($login, $senha, $callback)
        {
            $db_config = Zend_Registry::getInstance()->get('database');
            $dbAdapter = Zend_Db::factory($db_config->db->adapter, array(
                'host' => $db_config->db->params->host,
                'username' => $db_config->db->params->username,
                'password' => $db_config->db->params->password,
                'dbname' => $db_config->db->params->dbname,
                'driver_options' => array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            ));

            $adpter = new Zend_Auth_Adapter_DbTable($dbAdapter);
            $adpter->setTableName('usuario')
                ->setIdentityColumn('aluno.cpf')
                ->setCredentialColumn('usuario.senha');


            $select = $adpter->getDbSelect();
            $select->join('empresa', 'usuario.empresa_id = empresa.id', array('empresa.status' => 'status'));
            $select->joinLeft('aluno', 'usuario.id = aluno.usuario_id', array('aluno_nome' => 'nome', 'aluno_id' => 'id', 'aluno_status' => 'status'));

            $select->where('usuario.usuario_grupo_id = "' . Usuario::GRUPO_ALUNO . '"');
            $select->where('aluno.status != 0');
            $select->where('empresa.status = "' . Empresa::ATIVO . '"');


            $adpter->setIdentity($login)
                ->setCredential($senha);

            $auth = Zend_Auth::getInstance();
            $resultado = $auth->authenticate($adpter);


            if ($resultado->isValid()) {
                $info = $adpter->getResultRowObject(null, 'senha');
                if ($info->status == Usuario::ATIVO) {
                    $storage = $auth->getStorage();
                    $storage->write($info);

                    switch ($info->usuario_grupo_id) {
                        case Usuario::GRUPO_ALUNO:
                            $usuarioRepository = new UsuarioRepository();
                            $usuario = $usuarioRepository->getById($info->id);

                            if ($usuario->primeiro_acesso == NULL)
                                $usuario->primeiro_acesso = AppUtil::getCurrentDate();

                            $usuario->ultimo_acesso = AppUtil::getCurrentDate();
                            $usuario->save();

                            $this->_redirect($callback);
                    }
                } else {

                    $this->_helper->FlashMessenger(array('warning' => 'Token Inválido'));
                    $this->_redirect($this->view->baseUrl() . '/aluno/conta');

                }
            } else {
                $this->_helper->FlashMessenger(array('warning' => 'Token Inválido'));
                $this->_redirect($this->view->baseUrl() . '/aluno/conta');
            }
        }

    }
