<?php

class AlunoRepository extends RepositoryAbstract {

    public function __construct() {
        $this->obj = new Aluno();
        parent::__construct();
    }

    public function generateMatricula($empresa_id) {
        $q = Doctrine_Query::create()
                ->from(get_class($this->obj))
                ->select('matricula')
                ->where("empresa_id = ?", array($empresa_id))
                ->orderBy('matricula DESC')
                ->limit(1);
        return $q->fetchOne()->matricula + 1;
    }

}