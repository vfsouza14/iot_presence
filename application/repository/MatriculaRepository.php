<?php

class MatriculaRepository extends RepositoryAbstract {

    public function __construct() {
        $this->obj = new Matricula();
        parent::__construct();
    }

    public function getByAlunoId($aluno_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj))
            ->where("aluno_id = ?", array($aluno_id));
        return $q->fetchOne();
    }

    public function getByMatricula($matricula, $empresa_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where("p.matricula = ?", array($matricula))
            ->addWhere("p.empresa_id = ?", array($empresa_id));

        return $q->fetchOne();
    }

    public function getByCpfTotal($cpf){

        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u')
            ->where("p.cpf = ?", array($cpf))
            ->addWhere("u.status != ?", array(Usuario::EXCLUIDO))
            ->addWhere("p.status > 0");

        return $q->fetchOne();
    }

    public function getByCpf($cpf, $empresa_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u')
            ->where("p.cpf = ?", array($cpf))
            ->addWhere("u.status != ?", array(Usuario::EXCLUIDO))
            ->addWhere("p.empresa_id = ?", array($empresa_id))
            ->addWhere("p.status > 0");

        return $q->fetchOne();
    }

    public function getByEmailAndAcesso($email) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u')
            ->where("u.email = ?", array($email))
            ->addWhere("u.status != ?", array(Usuario::EXCLUIDO))
            ->addWhere("p.status > 0");

        return $q->fetchOne();
    }

    public function getByEmail($email, $empresa_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u')
            ->where("u.email = ?", array($email))
            ->addWhere("u.status != ?", array(Usuario::EXCLUIDO))
            ->addWhere("p.empresa_id = ?", array($empresa_id))
            ->addWhere("p.status > 0");

        return $q->fetchOne();
    }

    public function getByCpfAndAcesso($cpf, $acesso ) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u')
            ->where("p.cpf = ?", array($cpf))
            ->addWhere("p.status > 0")
            ->addWhere("u.status != ?", array(Usuario::EXCLUIDO));

        return $q->fetchOne();
    }

    public function matriculaExiste(Aluno $aluno){
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->select('p.matricula')
            ->where("p.matricula = ?", array($aluno->matricula))
            ->orderBy('p.matricula DESC')
            ->limit(1);

        if ($aluno->id > 0) {
            $q->addWhere("p.id != ?", array($aluno->id));
            $q->addWhere("p.status > 0");
        }

        return $q->count() > 0;
    }

    public function multipleAcesso($cpf) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where("p.cpf = ?", array($cpf))
            ->addWhere("p.status != 0");

        return $q->count() > 1;
    }

    public function multipleAcessoList($cpf) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where("p.cpf = ?", array($cpf))
            ->addWhere("p.status != 0");

        return $q->execute();
    }

    public function isExiste(Aluno $aluno) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u')
            ->where("p.empresa_id = ?", array($aluno->empresa_id))
            ->addWhere("u.status != ?", array(Usuario::EXCLUIDO))
            ->andWhere("p.cpf = ?", array($aluno->cpf))
            ->andWhere("p.sistema = ?", array($aluno->sistema));

        if ($aluno->id > 0) {
            $q->addWhere("p.id != ?", array($aluno->id));
            $q->addWhere("p.status > 0");
        }

        return $q->count() > 0;
    }

    public function getListDocumentosExigidos($aluno_id, $aluno_sexo) {
        $aluno_documentos = $this->_getDocumentosById($aluno_id);
        $documentos_atuais = str_replace(';', ',', $aluno_documentos[0]->documentos);

        $sql = "SELECT CDT.id, CDT.nome FROM curso_documento CD
                    INNER JOIN curso_documento_tipo CDT ON (CD.curso_documento_tipo_id = CDT.id OR CDT.id IN ('" . $documentos_atuais ."') )
                    INNER JOIN curso C ON CD.curso_id = C.id
                    INNER JOIN aluno_curso AC ON AC.curso_id = C.id ";

        if ($aluno_id > 0)
            $sql .=  " WHERE AC.aluno_id = " . $aluno_id . " AND (CDT.sexo = " . $aluno_sexo . " OR CDT.sexo = 0 )";

        $sql .= " GROUP BY CDT.id, CDT.nome ORDER BY CDT.ordem, CDT.nome ";

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $query->execute();

        return $query->fetchAll(Doctrine_Core::FETCH_CLASS);
    }

    public function getAlunosExportacao($offset){

        $q = Doctrine_Query::create()
            ->select('p.id as aluno_id, p.agenciador_id as agen_id, p.*, e.*, e.id as end_id, u.*, u.id as usu_id, u.status as usuario_status')
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Endereco e')
            ->innerJoin('p.Usuario u')
            ->innerJoin('p.AlunoCurso ac')
            ->innerJoin('ac.Curso c')
            ->innerJoin('c.CursoTipo ct')
            ->addWhere('ct.id = 3')
            ->limit(10)
            ->offset($offset);


        return $q->execute();

    }

    public function countAlunosExportacao(){

        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Endereco e')
            ->innerJoin('p.Usuario u')
            ->innerJoin('p.AlunoCurso ac')
            ->innerJoin('ac.Curso c')
            ->innerJoin('c.CursoTipo ct')
            ->addWhere('ct.id = 3');

        return $q->count();

    }

    public function getQuantidadeAlunoByStatus($empresa_id) {
        $q = Doctrine_Query::create()
            ->select('status, COUNT(*) AS quantidade')
            ->from(get_class($this->obj) . ' p')
            ->where("p.empresa_id = ?", array($empresa_id))
            ->groupBy('status');

        return $q->execute();
    }

    public function getAlunoNewsletter($empresa_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin("p.Usuario u")
            ->addWhere("p.status = ?", array(Aluno::SITUACAO_ATIVO))
            ->addWhere("p.empresa_id = ?", array($empresa_id))
            ->groupBy("u.email");

        return $q->execute();
    }

    public function getInadimplentesList(RepositoryFilter $repository_filter, RepositoryOrder $repository_order, $per_page) {

        $result = array();
        $page = ($repository_filter->getParamByField('page') <= 0) ? 1 : $repository_filter->getParamByField('page');
        $aluno_situacao = ($repository_filter->getParamByField('situacao')) ? $repository_filter->getParamByField('situacao') : ALUNO::SITUACAO_INADIMPLENTES;
        $busca_data = Null;

        if($repository_filter->getParamByField('data_primeiro_vencimento_inicial') && $repository_filter->getParamByField('data_primeiro_vencimento_final')){
            $busca_data = TRUE;
            $final_data = $repository_filter->getParamByField('data_primeiro_vencimento_final');
        }elseif($repository_filter->getParamByField('data_primeiro_vencimento_inicial') && !$repository_filter->getParamByField('data_primeiro_vencimento_final')){
            $busca_data = TRUE;
            $final_data = date("Y-m-d");
        }elseif($repository_filter->getParamByField('data_primeiro_vencimento_final')){
            $busca_data = TRUE;
            $final_data = $repository_filter->getParamByField('data_primeiro_vencimento_final');
        }

        $sql = "SELECT a.id AS aluno_id, 
	                   a.nome AS aluno_nome, 
	                   a.cpf AS aluno_cpf,
                       a.status AS aluno_status,
                       SUM(IF(l.status='".Lancamento::ATIVO."', 1, 0)) AS vencidos,
                       MIN(CASE WHEN l.status='".Lancamento::ATIVO."' THEN l.vencimento_data END) AS primeiro_venc

                FROM aluno a LEFT JOIN lancamento l ON a.id = l.aluno_id 

                WHERE 
                     a.status = '".$aluno_situacao."' AND
                     l.pagamento_data IS NULL AND
                     l.vencimento_data < '".date("Y-m-d")."' AND
                     IF(l.status = '".Lancamento::ATIVO."','".Lancamento::ATIVO."','".Lancamento::EXCLUIDO."')";

        if($repository_filter->getParamByField('nome'))
            $sql .= " AND a.nome LIKE "."'%".$repository_filter->getParamByField('nome')."%'";

        if($repository_filter->getParamByField('cpf'))
            $sql .= " AND a.cpf LIKE "."'%".$repository_filter->getParamByField('cpf')."%'";

        $sql .= " GROUP BY a.id";

        if($busca_data){
            if($aluno_situacao==ALUNO::SITUACAO_INADIMPLENTES){
                $sql .= " HAVING primeiro_venc BETWEEN '" . AppUtil::convertStringToDate($repository_filter->getParamByField('data_primeiro_vencimento_inicial')) . "' AND '" . AppUtil::convertStringToDate($final_data) . "'";
            }else{
                $sql .= " HAVING MIN(primeiro_venc) BETWEEN '" . AppUtil::convertStringToDate($repository_filter->getParamByField('data_primeiro_vencimento_inicial'))  . "' AND '" . AppUtil::convertStringToDate($final_data) . "'";
            }
        }

        if($aluno_situacao==ALUNO::SITUACAO_INADIMPLENTES){
            if($busca_data && $repository_filter->getParamByField('numero_vencidos')!=Null){
                $sql .= " AND vencidos = " . $repository_filter->getParamByField('numero_vencidos');
            }elseif($repository_filter->getParamByField('numero_vencidos')!=Null){
                $sql .= " HAVING vencidos = " . $repository_filter->getParamByField('numero_vencidos');
            }
        }else{
            if($busca_data && $repository_filter->getParamByField('numero_vencidos')!=Null){
                $sql .= " AND SUM(IF(l.status='".Lancamento::ATIVO."', 1, 0)) = " . $repository_filter->getParamByField('numero_vencidos');
            }elseif($repository_filter->getParamByField('numero_vencidos')!=Null){
                $sql .= " HAVING SUM(IF(l.status='".Lancamento::ATIVO."', 1, 0)) = " . $repository_filter->getParamByField('numero_vencidos');
            }
        }

        $sql_count = "SELECT COUNT(aluno_id) AS quantidade FROM (" . $sql . ") AS consulta";
        $sql .= " ORDER BY a.nome ASC";
        $sql .= " LIMIT ". $per_page;
        $sql .= " OFFSET ". ($page-1) * $per_page;

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql_count);
        $query->execute();
        $count = $query->fetchAll(Doctrine_Core::FETCH_CLASS);

        $query = $db->prepare($sql);
        $query->execute();
        $result['items'] = $query->fetchAll(Doctrine_Core::FETCH_CLASS);

        $paginator = Zend_Paginator::factory(intval($count[0]->quantidade));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($per_page);
        $paginator->setPageRange($per_page);
        $result['paginator'] = $paginator;

        return $result;

    }

    public function getAlunoByCpfOrEmail($cpf, $email) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u');

        if ($cpf != "")
            $q->where("p.cpf = ?", array($cpf));

        if ($email != "")
            $q->orWhere("u.email = ?", array($email));

        $q->addWhere('u.status = ' . Aluno::ATIVO);

        return $q->fetchOne();
    }

    public function getAlunoByEmail($email) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u');

        $q->Where("u.email = ?", array($email));
        $q->addWhere('u.status = ' . Aluno::ATIVO);

        return $q->fetchOne();
    }

    public function getListFromSelect($empresa_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where("p.empresa_id = ?", array($empresa_id))
            ->addWhere('p.status > ' . 0)
            ->orderBy('p.nome');

        return $q->execute();
    }

    public function getListByCPF($id, $empresa_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where("p.empresa_id = ?", array($empresa_id))
            ->addWhere("p.id = ?", array($id))
            ->orderBy('p.nome');

        return $q->execute();
    }

    public function getListEmFormacao($repository_filter) {
        $sql = "SELECT aluno_id, aluno_nome, aluno_matricula, aluno_cpf, curso_nome, curso_tipo_nome,
                        tcc_status, tcc_id, aluno_tcc_historico_id, aluno_tcc_historico_status, assinado_em, tempo_minimo_curso
                FROM (
                        SELECT ct.nome AS curso_tipo_nome, c.nome AS curso_nome, a.nome AS aluno_nome, a.id AS aluno_id, a.cpf AS aluno_cpf,
                                a.matricula AS aluno_matricula, atcc.status AS tcc_status, atcc.id AS tcc_id, ath.id AS aluno_tcc_historico_id,
                                ath.status AS aluno_tcc_historico_status, cm.assinado_em AS assinado_em, cd.tempo_minimo_curso AS tempo_minimo_curso
                        FROM aluno a
                        INNER JOIN aluno_curso ac ON a.id = ac.aluno_id
                        INNER JOIN contrato_matricula cm on cm.aluno_curso_id = ac.id
                        INNER JOIN curso c ON c.id = ac.curso_id
                        INNER JOIN curso_detalhe cd ON c.id = cd.curso_id
                        INNER JOIN curso_tipo ct ON ct.id = c.curso_tipo_id
                        LEFT JOIN aluno_tcc atcc ON atcc.aluno_id = a.id
                        LEFT JOIN aluno_tcc_historico ath on ath.id =
                                (SELECT max(ath.id)
                                FROM aluno_tcc_historico ath
                                WHERE atcc.id = ath.aluno_tcc_id)
                        WHERE 1 = 1
                    /*AND cm.assinado_em <= (SELECT date_sub(now(), interval cd.tempo_minimo_curso month)
                                            FROM curso_detalhe cd
                                            WHERE c.id = cd.curso_id)*/
                    AND a.id NOT IN (
                                SELECT aluno_id
                                FROM aluno_documento_analise
                                WHERE status != 2
                                AND aluno_id IS NOT NULL
                                GROUP BY aluno_id)
                    AND ac.id IN (
                        SELECT aluno_curso_id
                        FROM (
                            SELECT an.aluno_id, ac.curso_id, ac.id AS aluno_curso_id, COUNT(nota) AS avaliacoes, SUM(nota) AS nota_total, (7 * COUNT(nota)) AS media, SUM(nota) - (7 * COUNT(nota)) AS diferenca
                            FROM aluno_nota an
                            INNER JOIN curso_atividade ca ON ca.id = an.curso_atividade_id
                            INNER JOIN aluno_curso ac ON ac.aluno_id = an.aluno_id AND ac.curso_id = ca.curso_id
                            GROUP BY aluno_id, curso_id) AS notas
                        WHERE diferenca >= 0)
                    /*AND a.id IN (
                                SELECT aluno_id
                                FROM aluno_estagio_historico aeh
                                INNER JOIN aluno_estagio ae ON ae.id = aeh.aluno_estagio_id
                                WHERE aeh.status IN (". AlunoEstagioHistorico::STATUS_APROVADO .",". AlunoEstagioHistorico::STATUS_DISPENSADO .")
                                GROUP BY ae.aluno_id)*/
                    AND a.id NOT IN (
                                SELECT aluno_id
                                FROM lancamento
                                WHERE pagamento_data IS NULL
                                AND tipo IN (". Lancamento::TIPO_RECEITA . "," . Lancamento::MENSALIDADE . ")
                                AND aluno_id IS NOT NULL
                                GROUP BY aluno_id)
                    AND a.status IN (" . Aluno::SITUACAO_ATIVO . "," . Aluno::SITUACAO_FORMADOS . ")
                    AND ac.conclusao_data IS NULL
                    AND ac.trancado_data IS NULL
                    AND c.curso_nucleo_comum IS NOT NULL
                    AND ath.id IS NOT NULL) aa
                WHERE 1 = 1";

        if (!is_null($repository_filter) && !$repository_filter->isEmpty()) {
            $filter_array = array();

            foreach ($repository_filter->getFiltersGeneric() as $filterGeneric) {
                $filter_array[] = $filterGeneric;
            }

            foreach ($repository_filter->getFilters() as $filter) {
                $filter_array[] = str_replace("?", "'" . $filter[1] . "'", $filter[0]);
            }

            if (count($filter_array) > 0)
                $sql .= " AND " . implode(" AND ", $filter_array);
        }

        $sql .= " ORDER BY aluno_nome";

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $query->execute();

        return $query->fetchAll(Doctrine_Core::FETCH_CLASS);
    }

    public function atualizarSituacao($empresa, $aluno_id = 0) {

        $sql = 'UPDATE aluno a
                    INNER JOIN usuario u ON a.usuario_id = u.id
                        SET a.status = ' . Aluno::ATIVO . ',
                            u.status = ' . Usuario::ATIVO . '
                WHERE a.status = ' . Aluno::SITUACAO_INADIMPLENTES;

        if ($aluno_id > 0)
            $sql .= ' AND a.id = ' . $aluno_id;

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $result = $query->execute();


        /**
         * Atualizar Inadimplentes de Mensalidade de [ATIVO | ATIVO] para [INADIMPLENTE | INATIVO]
         * Limite de 20 dias
         */
        $dias_mensalidade = $empresa->EmpresaConfiguracao[0]->getDiasInadimplenciaMensalidade(); //20;
        $date_limit = date("Y-m-d", strtotime(date("Y-m-d") . "-" . $dias_mensalidade . " day"));
        $sql = 'UPDATE aluno a
                    INNER JOIN usuario u ON a.usuario_id = u.id
                    INNER JOIN lancamento l ON a.id = l.aluno_id
                      SET a.status = ' . Aluno::SITUACAO_INADIMPLENTES . ',
                          u.status = ' . Usuario::INATIVO . '
                    WHERE l.tipo = ' . Lancamento::TIPO_RECEITA . '
                      AND l.pagamento_data IS NULL
                      AND l.vencimento_data < "' . $date_limit . '"
                      AND l.plano_de_contas_id = ' . PlanoDeContas::MENSALIDADE . '
                      AND a.status IN (' . Aluno::SITUACAO_ATIVO . ')
                      AND l.status = ' . Lancamento::ATIVO . '
                      AND u.status = ' . Usuario::ATIVO;

        if ($aluno_id > 0)
            $sql .= ' AND a.id = ' . $aluno_id;

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $result = $query->execute();

        /**
         * Atualizar Inadimplentes de Taxa Inscrição de [ATIVO | ATIVO] para [INADIMPLENTE | INATIVO]
         * Limite de 3 dias (Devido ao final de semana e feriados)
         */
        $dias_inscricao = $empresa->EmpresaConfiguracao[0]->getDiasInadimplenciaInscricao(); //3;
        $date_limit = date("Y-m-d", strtotime(date("Y-m-d") . "-" . $dias_inscricao . " day"));
        //$date_limit = date("Y-m-d");
        $sql = 'UPDATE aluno a
                    INNER JOIN usuario u ON a.usuario_id = u.id
                    INNER JOIN lancamento l ON a.id = l.aluno_id
                      SET a.status = ' . Aluno::SITUACAO_INADIMPLENTES . ',
                          u.status = ' . Usuario::INATIVO . '
                    WHERE l.tipo = ' . Lancamento::TIPO_RECEITA . '
                      AND l.pagamento_data IS NULL
                      AND l.vencimento_data < "' . $date_limit . '"
                      AND l.plano_de_contas_id = ' . PlanoDeContas::TAXA_INSCRICAO . '
                      AND a.status IN (' . Aluno::SITUACAO_ATIVO . ')
                      AND l.status = ' . Lancamento::ATIVO . '
                      AND u.status = ' . Usuario::ATIVO;

        if ($aluno_id > 0)
            $sql .= ' AND a.id = ' . $aluno_id;

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $result = $query->execute();

        return $result;
    }

    public function getPercentual($empresa_id) {
        $q = Doctrine_Query::create()
            ->select('status, COUNT(*) AS quantidade')
            ->from(get_class($this->obj) . ' p')
            ->where("p.empresa_id = ?", array($empresa_id))
            ->groupBy('status');

        return $q->execute();
    }

    private function _getDocumentosById($aluno_id) {
        if ($aluno_id > 0) {
            $sql = "SELECT documentos FROM aluno WHERE id = " . $aluno_id;

            $db = Doctrine_Manager::getInstance()->getCurrentConnection();
            $query = $db->prepare($sql);
            $query->execute();

            return $query->fetchAll(Doctrine_Core::FETCH_CLASS);
        }

        return NULL;
    }

    public function getByDocumentoExistente() {

        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where('p.documentos is not null');

        return $q->execute();
    }


    public function getAjaxListByFilter($ajax_filter) {

        $limit = 10;
        $offset = $ajax_filter['start'];
        $search = $ajax_filter['search']['value'];

        if (trim($search) == "")
            return array();

        $sql = "SELECT a.id AS aluno_id, a.nome AS aluno_nome, a.cpf AS aluno_cpf, u.email AS aluno_email 
                    FROM aluno a 
                        INNER JOIN usuario u ON a.usuario_id = u.id
                WHERE u.status != 3
                    AND (a.nome LIKE '%" . $search . "%' OR a.cpf LIKE '%" . $search . "%' OR u.email LIKE '%" . $search . "%')
                ";

        $sql .= " ORDER BY a.nome ASC";
        $sql .= " LIMIT " . $limit;
        $sql .= " OFFSET " . $offset;

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $query->execute();

        return $query->fetchAll(Doctrine_Core::FETCH_CLASS);
    }

    public function getByUsuario($usuario) {
        $q = Doctrine_Query::create()

            ->from(get_class($this->obj) . ' p')
            ->where("p.usuario_id = ?", array($usuario));

        return $q->fetchOne();
    }

    public function getByPerfilPoloId($polo_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->addWhere('p.polo_id = ?', $polo_id);


        return $q->fetchOne();
    }

    public function getByListAlunos() {

        $sql = "SELECT id
                FROM aluno
                WHERE status IN (1,2,4,5,7,8);";

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $query->execute();

        return $query->fetchAll(Doctrine_Core::FETCH_CLASS);
    }
    public function getByTelefone() {

        $sql = "SELECT telefone
                FROM aluno
                WHERE id = '88'";

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql);
        $query->execute();

        return $query->fetchAll(Doctrine_Core::FETCH_CLASS);
    }

    public function getByListGroup(RepositoryFilter $repository_filter = null, $paginacao) {

        $result = array();
        $page = ($repository_filter->getParamByField('page') <= 0) ? 1 : $repository_filter->getParamByField('page');

        $sql = "SELECT a.id as aluno_id, a.cpf as aluno_cpf, a.nome as aluno_nome, 
                a.rg as aluno_rg, a.matricula as aluno_matricula,
                u.email as email_aluno, a.data_cadastro as aluno_cadastro,
                a.status as aluno_status, a.lista_negra as aluno_lista_negra,    
                a.sistema as aluno_sistema, u.senha as usuario_senha, a.usuario_id as aluno_usuario_id ,a.empresa_contribuinte_cnpj as aluno_empresa_contribuinte_cnpj,
                u.status as usuario_status, u.primeiro_acesso as usuario_primeiro_acesso
                FROM aluno a
                INNER JOIN usuario u ON a.usuario_id = u.id
                LEFT JOIN aluno_curso ac ON ac.aluno_id = a.id
                LEFT JOIN curso c ON ac.curso_id = c.id
                LEFT JOIN curso_tipo ct ON ct.id = c.curso_tipo_id
                WHERE 1
                ";

        $nome_replace = str_replace("'", "%", $repository_filter->getParamByField('nome'));

        if(!empty($repository_filter->getParamByField('nome')))
            $sql .= " AND a.nome like '%". $nome_replace ."%'";


        if (strlen($repository_filter->getParamByField('data_cadastro_inicial')) > 0 && strlen($repository_filter->getParamByField('data_cadastro_final')) > 0)
            $sql .= " AND DATE(a.data_cadastro) BETWEEN '" . AppUtil::convertStringToDate($repository_filter->getParamByField('data_cadastro_inicial')) . "' AND '" . AppUtil::convertStringToDate($repository_filter->getParamByField('data_cadastro_final')) . "' ";
        else if (strlen($repository_filter->getParamByField('data_cadastro_inicial')) > 0)
            $sql .= " AND DATE(a.data_cadastro) >= '" . AppUtil::convertStringToDate($repository_filter->getParamByField('data_cadastro_inicial')) . "' ";
        else if (strlen($repository_filter->getParamByField('data_cadastro_final')) > 0)
            $sql .= " AND DATE(a.data_cadastro) <= '" . AppUtil::convertStringToDate($repository_filter->getParamByField('data_cadastro_final')) . "' ";

        $sql .= " AND a.cpf like '%". $repository_filter->getParamByField('cpf') ."%'";
        $sql .= " AND a.rg like '%". $repository_filter->getParamByField('rg') ."%'";
        $sql .= " AND a.matricula like '%". $repository_filter->getParamByField('matricula') ."%'";
        $sql .= " AND u.email like '%". $repository_filter->getParamByField('email') ."%'";

        if($repository_filter->getParamByField('curso_tipo_id') > 0)
            $sql .= " AND ct.id = " . $repository_filter->getParamByField('curso_tipo_id');

        if($repository_filter->getParamByField('situacao') > 0)
            $sql .= " AND a.status like '%". $repository_filter->getParamByField('situacao') . "%'";

        if ($repository_filter->getParamByField('status') == Usuario::EXCLUIDO)
            $sql .= " AND u.status = " . $repository_filter->getParamByField('status');
        else
            $sql .= " AND a.status != 0";

        if($repository_filter->getParamByField('lista_negra') == 1)
            $sql .= " AND a.lista_negra = 1";

        if(!Empresa::isMaster())
            $sql .= " AND a.empresa_id = " . Empresa::getLogged()->id;

        if($repository_filter->getParamByField('polo_presencial') > 0)
            $sql .= " AND a.polo_id =" . $repository_filter->getParamByField('polo_presencial');

        $sql .= " GROUP BY a.id";

        $sql_count = "SELECT COUNT(*) AS quantidade FROM (" . $sql . ") AS consulta";
        $sql .= " ORDER BY a.nome ASC";
        $sql .= " LIMIT ". $paginacao;
        $sql .= " OFFSET ". ($page-1) * $paginacao;

        $db = Doctrine_Manager::getInstance()->getCurrentConnection();
        $query = $db->prepare($sql_count);
        $query->execute();
        $count = $query->fetchAll(Doctrine_Core::FETCH_CLASS);
        $query = $db->prepare($sql);
        $query->execute();
        $result['items'] = $query->fetchAll(Doctrine_Core::FETCH_CLASS);
        $paginator = Zend_Paginator::factory(intval($count[0]->quantidade));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($paginacao);
        $paginator->setPageRange($paginacao);
        $result['paginator'] = $paginator;

        return $result;
    }

    public function getListByOffset($offset){
        set_time_limit(300);
        ini_set('memory_limit', '2048M');
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->limit(5000)
            ->offset($offset);

        return $q->execute();
    }


}