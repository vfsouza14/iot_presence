<?php

class ProfessorRepository extends RepositoryAbstract {

    public function __construct() {
        $this->obj = new Professor();
        parent::__construct();
    }
}