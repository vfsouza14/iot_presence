<?php

class RepositoryFilter {

    private $_filters = array();
    private $_filtersGeneric = array();
    private $_filtersJoin = array();
    private $_filtersLeftJoin = array();
    private $_params = array();

    public function __construct($params = null) {
        $this->_params = $params;
    }

    public function addFilter($field, $value) {
        $this->_filters[] = array($field, $value);
    }

    public function addGenericFilter($filter) {
        $this->_filtersGeneric[] = $filter;
    }

    public function addNullFilter($field) {
        $this->addGenericFilter($field . ' IS NULL');
    }

    public function addNotNullFilter($field) {
        $this->addGenericFilter($field . ' IS NOT NULL');
    }

    public function addJoinFilter($table, $field = null, $value = 0) {
        $this->_filtersJoin[] = $table;
        if ($value > 0) {
            $this->addFilter($field . ' = ?', $value);
        }
    }

    public function addLeftJoinFilter($table, $field = null, $value = 0) {
        $this->_filtersLeftJoin[] = $table;
        if ($value > 0) {
            $this->addFilter($field . ' = ?', $value);
        }
    }

    public function addTextFilter($field, $value) {
        if (strlen($value) > 0)
            $this->addFilter($field . ' LIKE ?', '%' . trim($value) . '%');
    }

    public function addDateFilter($field, $value) {
        if (strlen($value) > 0)
            $this->addFilter($field . ' = ?', AppUtil::convertStringToDate($value));
    }

    public function addDateBetweenFilter($field, $date1, $date2) {
        if (strlen($date1) > 0 && strlen($date2) > 0)
            $this->addGenericFilter($field . ' BETWEEN "' . AppUtil::convertStringToDate($date1) . '" AND "' . AppUtil::convertStringToDate($date2) . '"');

        else if (strlen($date1) > 0)
            $this->addGenericFilter($field . ' >= "' . AppUtil::convertStringToDate($date1) . '"');

        else if (strlen($date2) > 0)
            $this->addGenericFilter($field . ' <= "' . AppUtil::convertStringToDate($date2) . '"');
    }

    public function addSelectFilter($field, $value) {
        if ($value > 0)
            $this->addFilter($field . ' = ?', $value);
    }

    public function getFilters() {
        return $this->_filters;
    }

    public function getFiltersGeneric() {
        return $this->_filtersGeneric;
    }

    public function getFiltersJoin() {
        return $this->_filtersJoin;
    }

    public function getFiltersLeftJoin() {
        return $this->_filtersLeftJoin;
    }

    public function isEmpty() {
        return is_null($this->_filters) && count($this->_filters) <= 0;
    }

    public function getParamByField($field) {
        return $this->_params[$field];
    }

    public function setParamByField($field, $value) {
        return $this->_params[$field] = $value;
    }

    public function getParamString($p_remove = null) {
        $propert_remove = array("controller", "action", "module", "page");

        foreach ($propert_remove as $remove) {
            unset($this->_params[$remove]);
        }

        foreach ($p_remove as $remove) {
            unset($this->_params[$remove]);
        }

        return http_build_query($this->_params);
    }

    public static function getUrlPage($url, $index, $url_part) {
        $pos = strpos($url, '?');
        if ($pos === false) {
            return $url . "?page=" . $index . '&' . $url_part;
        } else {
            $url = substr($url, 0, strpos($url, '?'));
            return $url . "?page=" . $index . '&' . $url_part;
        }
    }

    public static function getUrlFilter($remove_params = FALSE) {
        $url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();

        if ($remove_params) {
            $pos = strpos($url, '?');
            if ($pos > 0) {
                $url = substr($url, 0, $pos);
            }
        }

        return $url;
    }

    public function setFilterSession() {
        $url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $url = str_replace('/', '_', strtok($url, '?'));
        $_SESSION['filter_session'][$url] = $this->getParamString();
    }

    public static function getFilterSession() {
        $url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $url = str_replace('/', '_', strtok($url, '?'));
        $params = $_SESSION['filter_session'][$url];

        parse_str($params, $params_arr);

        return $params_arr;
    }

}
