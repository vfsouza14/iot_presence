<?php

class UsuarioRepository extends RepositoryAbstract {

    public function __construct() {
        $this->obj = new Usuario();
        parent::__construct();
    }

    public function isExiste($administrador) {
        $q = Doctrine_Query::create()
                ->from(get_class($this->obj) . ' p')
                ->innerJoin('p.Usuario u')
                ->where("u.email = ?", array($administrador->Usuario->email))
                ->addWhere("u.status != ?", array(Usuario::EXCLUIDO))
                ->addWhere("p.empresa_id = ?", array($administrador->empresa_id));

        if ($administrador->Usuario->id > 0)
            $q->addWhere("u.id != ?", array($administrador->Usuario->id));

        return $q->count() > 0;
    }

    public function getByEmail($email) {
        $q = Doctrine_Query::create()
                ->from(get_class($this->obj) . ' p')
                ->where("p.email = ?", array($email));
        return $q->fetchOne();
    }
    public function getByUsuario($usuario_id) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where("p.usuario_id = ?", array($usuario_id));
        return $q->fetchOne();
    }

    public function getByUsuarioId($usuario) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->where("p.usuario_id = ?", array($usuario));
        return $q->fetchOne();
    }

    public function getByUsuarioIdEmail($usuario_id, $email) {
        $q = Doctrine_Query::create()
            ->from(get_class($this->obj) . ' p')
            ->innerJoin('p.Usuario u')
            ->addWhere("u.email = ?", array($email))
            ->addWhere("p.usuario_id = ?", array($usuario_id));
        return $q->fetchOne();
    }

}
