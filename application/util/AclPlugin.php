<?php

class AclPlugin extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        if (($request->getModuleName() == 'site' && $request->getControllerName() != 'error')
                && ($request->getModuleName() == 'site' && $request->getControllerName() != 'external-auth')
            && ($request->getModuleName() == 'site' && $request->getControllerName() != 'acl')
            ) {
            $r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
            $r->gotoUrl(Zend_Controller_Front::getInstance()->getBaseUrl() . '/catalogo')->redirectAndExit();
        }

        if ($request->getControllerName() == 'auth'
                OR $request->getControllerName() == 'error'
                OR ( $request->getModuleName() == 'administrador' && $request->getControllerName() == 'conta'))
            return;

        if ($request->getModuleName() == 'api') {
            $this->_verifyAuthenticate($request);
            return;
        }

        $auth = Zend_Auth::getInstance();

        $usuario = $auth->getIdentity()->id;
        $module = $request->getModuleName();

        if (!$auth->hasIdentity()) {

            if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
                $request->setModuleName($module)
                ->setControllerName('conta')
                ->setActionName('index');
            }

            $r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
            $r->gotoUrl(Zend_Controller_Front::getInstance()->getBaseUrl() . '/' . $module . '/conta/')->redirectAndExit();

        }
    }

    private function _login($login, $senha) {
        $db_config = Zend_Registry::getInstance()->get('database');
        $dbAdapter = Zend_Db::factory($db_config->db->adapter, array(
            'host' => $db_config->db->params->host,
            'username' => $db_config->db->params->username,
            'password' => $db_config->db->params->password,
            'dbname' => $db_config->db->params->dbname,
            'driver_options' => array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
            )
        ));

        $adpter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $adpter->setTableName('usuario')
            ->setIdentityColumn('usuario.email')
            ->setCredentialColumn('usuario.senha');


        $select = $adpter->getDbSelect();
        $select->where('usuario.tipo = "' . Usuario::ADMINISTRADOR . '"');
        $select->where('usuario.status = "' . Usuario::ATIVO . '"');

        $adpter->setIdentity($login)
            ->setCredential($senha);

        $auth = Zend_Auth::getInstance();
        $auth->authenticate($adpter);

        $info = $adpter->getResultRowObject(null, 'senha');
        $storage = $auth->getStorage();
        $storage->write($info);

        $token = AuthUtil::tokenGenerate($info->id, $info->email, $info->usuario_grupo_id);
        SessionUtil::setAccessToken($token->access_token);

    }

}
