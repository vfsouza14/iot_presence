<?php

class AppUtil {

    //put your code here
    public static function autoload($className) {
        require_once $className . ".php";
        return true;
    }

    public static function getCurrentDate($format = 'Y-m-d H:i:s') {
        return date($format, time());
    }

    public static function getWeekNumber() {
        $w = date('w', time());

        return $w;
    }

    public static function setAddDay($add_dia)
    {
        $data = date("Y-m-d");
        $data_atualizada = date("Y-m-d", strtotime(date("Y-m-d H:i:s", strtotime($data)) . "+" . $add_dia . " day"));

        $data_util = AppUtil::getNextDayUtil($data_atualizada);

        $data_formatada = implode('/', array_reverse(explode('-', $data_util)));

        return $data_formatada;
    }

    public static function setSubHour($sub_seconds) {
        $data = date("Y-m-d H:i:s");
        $timestamp = strtotime($data);
        $time = $timestamp - ($sub_seconds * 60 * 60);
        return date("Y-m-d H:i:s", $time);
    }

    public static function getNextDayUtil($data, $saida = 'Y-m-d') {

        $timestamp = strtotime($data);

        $dia = date('N', $timestamp);

        if ($dia >= 6) {
            $timestamp_final = $timestamp + ((8 - $dia) * 3600 * 24);
        } else {
            $timestamp_final = $timestamp;
        }

        return date($saida, $timestamp_final);
    }

    public static function getDiferencaDias($data_inicial, $data_final) {
        $time_inicial = strtotime($data_inicial);
        $time_final = strtotime($data_final);

        $diferenca = $time_final - $time_inicial;

        $dias = (int) round($diferenca / (60 * 60 * 24));

        return $dias;
    }

    public static function getDiferencaMeses($data_inicial, $data_final) {
        $time_inicial = strtotime($data_inicial);
        $time_final = strtotime($data_final);

        $diferenca = $time_final - $time_inicial;

        $meses = (int) round($diferenca / (30 * 60 * 60 * 24));

        return $meses;
    }

    public static function setWordLower($str) {
        return mb_strtolower(trim($str), 'UTF-8');
    }

    public static function setWordUpper($str) {
        return mb_strtoupper($str, 'UTF-8');
    }

    public static function setFirstUpWord($str) {
        $excecoes = array("e", "de", "da", "do", "dos", "das");
        $texto = mb_strtolower(AppUtil::setWordLower($str));
        $texto_explodido = explode(" ", $texto);
        foreach ($texto_explodido as $palavra) {
            if(!in_array($palavra, $excecoes)){
                $texto_formatado[] = mb_convert_case($palavra, MB_CASE_TITLE, "UTF-8");
            }else {
                $texto_formatado[] = $palavra;
            }
        }
        return implode(" ", $texto_formatado);
    }

    public static function getIdUser() {
        return Zend_Auth::getInstance()->getIdentity()->id;
    }

    public static function getGroupUser() {
        return Zend_Auth::getInstance()->getIdentity()->usuario_grupo_id;
    }

    public static function gerar_senha($tamanho, $maiuscula, $minuscula, $numeros, $codigos) {
        $maius = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
        $minus = "abcdefghijklmnopqrstuwxyz";
        $numer = "0123456789";
        $codig = '!@#$%&*()-+.,;?{[}]^><:|';

        $base = '';
        $base .= ($maiuscula) ? $maius : '';
        $base .= ($minuscula) ? $minus : '';
        $base .= ($numeros) ? $numer : '';
        $base .= ($codigos) ? $codig : '';

        srand((float) microtime() * 10000000);
        $senha = '';
        for ($i = 0; $i < $tamanho; $i++) {
            $senha .= substr($base, rand(0, strlen($base) - 1), 1);
        }
        return $senha;
    }

    public static function getWeekDate($data) {
        $data_array = array_reverse(explode('-', $data));

        $d = mktime(0, 0, 0, $data_array[1], $data_array[0], $data_array[2]);
        $w = date('w', $d);

        return self::getSmallWeekText($w);
    }

    public static function getDayDate($data) {
        if (empty($data) || $data == '')
            return '';

        $data_array = array_reverse(explode('-', $data));

        return $data_array[0];
    }

    public static function getSmallMonthDate($data) {
        if (empty($data) || $data == '')
            return '';

        $data_array = array_reverse(explode('-', $data));

        return self::getSmallMonthText($data_array[1]);
    }

    private static function getSmallWeekText($week) {
        switch ($week) {
            case 0:
                return "DOM";
            case 1:
                return "SEG";
            case 2:
                return "TER";
            case 3:
                return "QUA";
            case 4:
                return "QUI";
            case 5:
                return "SEX";
            case 6:
                return "SAB";
        }
    }

    private static function getSmallMonthText($mon) {
        switch ($mon) {
            case 1:
                return "JAN";
            case 2:
                return "FEV";
            case 3:
                return "MAR";
            case 4:
                return "ABR";
            case 5:
                return "MAI";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AGO";
            case 9:
                return "SET";
            case 10:
                return "OUT";
            case 11:
                return "NOV";
            case 12:
                return "DEZ";
        }
    }

    public static function getFullMonthText($mon) {
        switch ($mon) {
            case 1:
                return "JANEIRO";
            case 2:
                return "FEVEREIRO";
            case 3:
                return "MARÇO";
            case 4:
                return "ABRIL";
            case 5:
                return "MAIO";
            case 6:
                return "JUNHO";
            case 7:
                return "JULHO";
            case 8:
                return "AGOSTO";
            case 9:
                return "SETEMBRO";
            case 10:
                return "OUTUBRO";
            case 11:
                return "NOVEMBRO";
            case 12:
                return "DEZEMBRO";
        }
    }

    public static function validaCpf($cpf) {
        // Verifiva se o número digitado contém todos os digitos
        $cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);

        // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
            return $result.="<li>O CPF informado é invalido.</li>";
        } else {   // Calcula os números para verificar se o CPF é verdadeiro
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return $result.="<li>O <b>CPF</b> informado é invalido.</li>";
                }
            }

            return "";
        }
    }
    
    public static function convertDateToString($data, $label_empty = "") {
        if (empty($data) || $data == '' || $data == '00/00/0000')
            return $label_empty;

        return join('/', array_reverse(explode('-', $data)));
    }

    public static function convertDateToDay($data, $label_empty = "") {
        if (empty($data) || $data == '' || $data == '00/00/0000')
            return $label_empty;

        $data_array = array_reverse(explode('-', $data));

        return $data_array[0];
    }

    public static function convertDateToText($data) {
        if (empty($data) || $data == '')
            return '-';

        $data_array = array_reverse(explode('-', $data));

        return $data_array[0] . self::getMonthText($data_array[1]) . $data_array[2];
    }

    public static function convertDateToMonthAndYear($date, $separator = '/') {
        if (empty($date) || $date == '')
            return '-';

        $date_array = array_reverse(explode('-', $date));

        return self::getTextMonth($date_array[1]) . $separator . $date_array[2];
    }

    public static function convertDateTimeToString($date_time, $type = 'datetime') {
        switch ($type) {
            case 'date':
                $data = substr($date_time, 0, 10);
                return self::convertDateToString($data);

            case 'time':
                $hora = substr($date_time, 11, 5);
                return $hora;

            default:
                $data = substr($date_time, 0, 10);
                $hora = substr($date_time, 11, 5);
                return join('/', array_reverse(explode('-', $data))) . ' ' . $hora;
        }
    }

    public static function convertDateTimeToDate($date_time) {
        $data = substr($date_time, 0, 10);
        return $data;
    }

    public static function getSmallMonth($mon) {
        switch ($mon) {
            case 1:
                return "JAN";
            case 2:
                return "FEV";
            case 3:
                return "MAR";
            case 4:
                return "ABR";
            case 5:
                return "MAI";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AGO";
            case 9:
                return "SET";
            case 10:
                return "OUT";
            case 11:
                return "NOV";
            case 12:
                return "DEZ";
        }
    }

    public static function getDateFull() {
        $meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
        $diasdasemana = array(1 => "Segunda-Feira", 2 => "Terça-Feira", 3 => "Quarta-Feira", 4 => "Quinta-Feira", 5 => "Sexta-Feira", 6 => "Sábado", 0 => "Domingo");
        $hoje = getdate();
        $dia = $hoje["mday"];
        $mes = $hoje["mon"];
        $nomemes = $meses[$mes];
        $ano = $hoje["year"];
        $diadasemana = $hoje["wday"];
        $nomediadasemana = $diasdasemana[$diadasemana];
        return "$nomediadasemana, $dia de $nomemes de $ano";
    }

    public static function getMeses() {
        $meses = array();
        $mes = array();


        for ($index = 1; $index <= 12; $index++) {
            $mes['id'] = $index;
            $mes['text'] = AppUtil::getTextMonth($index);
            $meses[] = $mes;
        }

        return $meses;
    }

    public static function getMes($data) {
        $mes = explode('-', $data)[1];

        return $mes;
    }

    public static function getAno($data) {
        $ano = explode('-', $data)[0];

        return $ano;
    }

    public static function getTextMonth($mon) {
        switch ($mon) {
            case 1:
                return "Janeiro";
            case 2:
                return "Fevereiro";
            case 3:
                return "Março";
            case 4:
                return "Abril";
            case 5:
                return "Maio";
            case 6:
                return "Junho";
            case 7:
                return "Julho";
            case 8:
                return "Agosto";
            case 9:
                return "Setembro";
            case 10:
                return "Outubro";
            case 11:
                return "Novembro";
            case 12:
                return "Dezembro";
        }
    }

    private static function getMonthText($mon) {
        switch ($mon) {
            case 1:
                return " de Janeiro de ";
            case 2:
                return " de Fevereiro de ";
            case 3:
                return " de Março de ";
            case 4:
                return " de Abril de ";
            case 5:
                return " de Maio de ";
            case 6:
                return " de Junho de ";
            case 7:
                return " de Julho de ";
            case 8:
                return " de Agosto de ";
            case 9:
                return " de Setembro de ";
            case 10:
                return " de Outubro de ";
            case 11:
                return " de Novembro de ";
            case 12:
                return " de Dezembro de ";
        }
    }

    public static function covertFloatToString($valor = 0, $maiusculas = false) {
        // verifica se tem virgula decimal
        if (strpos($valor, ",") > 0) {
            // retira o ponto de milhar, se tiver
            $valor = str_replace(".", "", $valor);

            // troca a virgula decimal por ponto decimal
            $valor = str_replace(",", ".", $valor);
        }
        $singular = array("centavo", "real", "mil", "milhÃ£o", "bilhÃ£o", "trilhÃ£o", "quatrilhÃ£o");
        $plural = array("centavos", "reais", "mil", "milhÃµes", "bilhÃµes", "trilhÃµes",
            "quatrilhÃµes");

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos",
            "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta",
            "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze",
            "dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "trÃªs", "quatro", "cinco", "seis",
            "sete", "oito", "nove");

        $z = 0;

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);
        $cont = count($inteiro);
        for ($i = 0; $i < $cont; $i++)
            for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
                $inteiro[$i] = "0" . $inteiro[$i];

        $fim = $cont - ($inteiro[$cont - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < $cont; $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                    $ru) ? " e " : "") . $ru;
            $t = $cont - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")
                $z++;
            elseif ($z > 0)
                $z--;
            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                        ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        if (!$maiusculas) {
            return($rt ? $rt : "zero");
        } elseif ($maiusculas == "2") {
            return (strtoupper($rt) ? strtoupper($rt) : "Zero");
        } else {
            return (ucwords($rt) ? ucwords($rt) : "Zero");
        }
    }

    public static function convertStringToDate($data) {
        if (empty($data) || $data == '') {
            return NULL;
        }

        return join('-', array_reverse(explode('/', $data)));
    }

    public static function convertStringToFloat($value, $remove_dec_point = TRUE) {
        if (empty($value))
            return 0;

        if ($remove_dec_point)
            $value = str_replace(".", "", $value);

        return str_replace(",", ".", $value);
    }

    public static function convertFloatToString($value, $dec_point = '', $label_zero = "", $casa_decimal = 2) {
        if (empty($value)) {
            return $label_zero;
        }

        // $value = round($value, 2); //Arredondamento Automático
        //$value = ceil($value); // Arredondamento p/ cima
        //$value = floor($value, 2);  Arredondamento p/ baixo

        return number_format($value, $casa_decimal, ',', $dec_point);
    }

    public function getAutor($autores) {
        $autor = array_filter(explode(";", $autores));

        return $autor;
    }

    public static function convertFloatToStringFluxoDeCaixa($value, $dec_point = '', $label_zero = "0,00") {

        if ($value < 0)
            return '<span style="color: red;">' . self::convertFloatToString($value * -1, '.', '-') . '</span>';

        return '<span style="color: blue;">' . self::convertFloatToString($value, '.', '-') . '</span>';
    }

    public static function convertFloatToStringColor($value) {
        $valor = self::convertFloatToString($value);

        if ($value < 0)
            return '<span style="color: red;">' . $valor . ' D</span>';

        return '<span style="color: blue;">' . $valor . ' C</span>';
        ;
    }

    public static function convertFloatToStringExtrato($value) {
        if ($value->tipo == Lancamento::TIPO_RECEITA)
            return $value->pagamento_valor;

        return $value->pagamento_valor * -1;
    }

    public static function convertLancamentoReceita($value) {
        if ($value->tipo == Lancamento::TIPO_RECEITA)
            return $value->pagamento_valor;

        return 0;
    }

    public static function convertLancamentoDespesa($value) {
        if ($value->tipo == Lancamento::TIPO_DESPESA)
            return $value->pagamento_valor;

        return 0;
    }

    public static function convertIntToTelefone($value) {
        if (empty($value))
            return;

        /*Verifica se é 0800*/
        if (substr($value, 0, 4) == '0800') {
            return substr($value, 0, 4) . ' ' . substr($value, 4, 3) . ' ' . substr($value, 7, 4);
        }

        $ddd = substr($value, 0, 2);

        if (strlen($value) == 10) {
            $tel_inicio = substr($value, 2, 4);
            $tel_fim = substr($value, 6, 4);
        }

        if (strlen($value) == 11) {
            $tel_inicio = substr($value, 2, 5);
            $tel_fim = substr($value, 7, 4);
        }


        return '(' . $ddd . ') ' . $tel_inicio . '-' . $tel_fim;
    }

    public static function convertIntToCPF($value) {
        if (empty($value))
            return;
        $parte1 = substr($value, 0, 3);
        $parte2 = substr($value, 3, 3);
        $parte3 = substr($value, 6, 3);
        $parte4 = substr($value, 9, 2);

        return $parte1 . '.' . $parte2 . '.' . $parte3 . '-' . $parte4;
    }

    public static function convertCPFToInt($value) {
        if (empty($value))
            return;

        $caracter_remove = array("(", ")", "-", " ", ".");
        return str_replace($caracter_remove, "", $value);
    }

    public static function convertCNPJToInt($value) {
        if (empty($value))
            return;

        $caracter_remove = array("/", "-", " ", ".");
        return str_replace($caracter_remove, "", $value);
    }

    public static function convertIntToCNPJ($value) {
        if (empty($value))
            return;
        $parte1 = substr($value, 0, 2);
        $parte2 = substr($value, 2, 3);
        $parte3 = substr($value, 5, 3);
        $parte4 = substr($value, 8, 4);
        $parte5 = substr($value, 12, 2);


        return $parte1 . '.' . $parte2 . '.' . $parte3 . '/' . $parte4 . '-' . $parte5;
    }

    public static function convertIntToCEP($value) {
        if (empty($value))
            return;
        $parte1 = substr($value, 0, 2);
        $parte2 = substr($value, 2, 3);
        $parte3 = substr($value, 5, 3);

        return $parte1 . '.' . $parte2 . '-' . $parte3;
    }

    public static function convertObjectListToArray($object_list, $field = "id") {
        $result = array();
        foreach ($object_list as $object) {
            $result[] = $object[$field];
        }

        return $result;
    }

    public static function resumeTexto($texto, $limite, $tres_p = '') {
        //Retorna o texto em plain/text
        $texto = self::somenteTexto($texto);

        if (strlen($texto) <= $limite)
            return $texto;
        return array_shift(explode('||', wordwrap($texto, $limite, '||'))) . $tres_p;
    }

    private static function somenteTexto($string) {
        //$trans_tbl = get_html_translation_table(HTML_ENTITIES);
        //$trans_tbl = array_flip($trans_tbl);
        return trim(strip_tags($string));
    }

    public static function getValorMulta($data_vencimento, $data_recebimento, $taxa_multa, $valor_original) {
        if ($taxa_multa <= 0)
            return 0;

        $dias_atraso = self::getDiasAtraso($data_vencimento, $data_recebimento);
        $taxa_multa = $taxa_multa / 100;
        return ($dias_atraso > 3) ? $taxa_multa * $valor_original : 0;
    }

    public static function getValorJuros($data_vencimento, $data_recebimento, $taxa_juros, $valor_original) {
        if ($taxa_juros <= 0)
            return 0;

        $dias_atraso = self::getDiasAtraso($data_vencimento, $data_recebimento);
        $taxa_juros = $taxa_juros / 100;
        return ($dias_atraso > 3) ? ($taxa_juros / 30 * $dias_atraso) * $valor_original : 0;
    }

    public static function getDiasAtraso($data_vencimento, $data_recebimento) {
        $data_vencimento = explode('-', $data_vencimento);
        $data_vencimento = mktime(0, 0, 0, $data_vencimento[1], $data_vencimento[2], $data_vencimento[0]);

        $data_recebimento = explode('-', $data_recebimento);
        $data_recebimento = mktime(0, 0, 0, $data_recebimento[1], $data_recebimento[2], $data_recebimento[0]);

        $diferenca = $data_recebimento - $data_vencimento;
        $diferenca = ($diferenca > 0 ) ? $diferenca : 0;

        return (int) floor($diferenca / (60 * 60 * 24));
    }

    public static function getFileView($obj, $path, $size, $imageSuffix = null, $createThumb = true, $extension = 'jpg') {
        if (file_exists(APPLICATION_UPLOAD_PATH . '/' . $path . '/' . AppUtil::getFileName($obj, $imageSuffix, $extension)))
            return self::_getImageThumb(Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/upload/' . $path . '/' . AppUtil::getFileName($obj, $imageSuffix, $extension), $size, $createThumb);


        return Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/img/' . $size . 'x' . $size . '.gif';
    }

    private static function _getImageThumb($img, $size, $createThumb = true) {
        if (!$createThumb)
            return $img;

        return Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/timthumb.php?src=' . $img . '&h=' . $size . '&w=' . $size;
    }

    public static function getImgResized($obj, $heigth, $width) {
        return Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/site/thumb.php?src=' . $obj . '&amp;h=' . $heigth . '&amp;w=' . $width;
    }

    public static function isFileExists($obj, $path, $imageSuffix = null, $extension = 'jpg') {
        if (file_exists(APPLICATION_UPLOAD_PATH . '/' . $path . '/' . AppUtil::getFileName($obj, $imageSuffix, $extension)))
            return true;

        return false;
    }

    public static function getImage($obj, $path) {
        return Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/upload/' . $path . '/' . self::getFileName($obj);
    }

    public static function getImageByName($name, $path) {
        return Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/upload/' . $path . '/' . $name . '.png';
    }

    public static function getFile($obj, $path, $imageSuffix = null, $extension = 'jpg') {
        return Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/upload/' . $path . '/' . self::getFileName($obj, $imageSuffix, $extension);
    }

    public static function getImageResized($obj, $path, $heigth, $width) {
        return Zend_Controller_Front::getInstance()->getBaseUrl() . '/content/site/thumb.php?src=' . self::getImage($obj, $path) . '&amp;h=' . $heigth . '&amp;w=' . $width;
    }

    public static function getFileName($obj, $imageSuffix = null, $extension = 'jpg') {
        if ($imageSuffix && $imageSuffix <> "")
            $imageSuffix = '_' . $imageSuffix;

        return md5($obj->id) . $imageSuffix . "." . $extension;
    }

    public static function getFileNameById($id, $imageSuffix = null, $extension = 'jpg') {
        if ($imageSuffix && $imageSuffix <> "")
            $imageSuffix = '_' . $imageSuffix;

        return md5($id) . $imageSuffix . "." . $extension;
    }

    /**
     * Translates a number to a short alhanumeric version
     *
     * Translated any number up to 9007199254740992
     * to a shorter version in letters e.g.:
     * 9007199254740989 --> PpQXn7COf
     *
     * specifiying the second argument true, it will
     * translate back e.g.:
     * PpQXn7COf --> 9007199254740989
     *
     * this function is based on any2dec && dec2any by
     * fragmer[at]mail[dot]ru
     * see: http://nl3.php.net/manual/en/function.base-convert.php#52450
     *
     * If you want the alphaID to be at least 3 letter long, use the
     * $pad_up = 3 argument
     *
     * In most cases this is better than totally random ID generators
     * because this can easily avoid duplicate ID's.
     * For example if you correlate the alpha ID to an auto incrementing ID
     * in your database, you're done.
     *
     * The reverse is done because it makes it slightly more cryptic,
     * but it also makes it easier to spread lots of IDs in different
     * directories on your filesystem. Example:
     * $part1 = substr($alpha_id,0,1);
     * $part2 = substr($alpha_id,1,1);
     * $part3 = substr($alpha_id,2,strlen($alpha_id));
     * $destindir = "/".$part1."/".$part2."/".$part3;
     * // by reversing, directories are more evenly spread out. The
     * // first 26 directories already occupy 26 main levels
     *
     * more info on limitation:
     * - http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/165372
     *
     * if you really need this for bigger numbers you probably have to look
     * at things like: http://theserverpages.com/php/manual/en/ref.bc.php
     * or: http://theserverpages.com/php/manual/en/ref.gmp.php
     * but I haven't really dugg into this. If you have more info on those
     * matters feel free to leave a comment.
     *
     * @author  Kevin van Zonneveld <kevin@vanzonneveld.net>
     * @author  Simon Franz
     * @author  Deadfish
     * @copyright 2008 Kevin van Zonneveld (http://kevin.vanzonneveld.net)
     * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
     * @version   SVN: Release: $Id: alphaID.inc.php 344 2009-06-10 17:43:59Z kevin $
     * @link    http://kevin.vanzonneveld.net/
     *
     * @param mixed   $in    String or long input to translate
     * @param boolean $to_num  Reverses translation when true
     * @param mixed   $pad_up  Number or boolean padds the result up to a specified length
     * @param string  $passKey Supplying a password makes it harder to calculate the original ID
     *
     * @return mixed string or long
     */
    public static function alphaID($in, $to_num = false, $pad_up = 4, $passKey = 'ofertassim') {
        if ($in > 50000) {
            $pad_up += intval($in / 50000);
        }// acada 50 mil sobe 1
        $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if ($passKey !== null) {
            // Although this function's purpose is to just make the
            // ID short - and not so much secure,
            // with this patch by Simon Franz (http://blog.snaky.org/)
            // you can optionally supply a password to make it harder
            // to calculate the corresponding numeric ID

            for ($n = 0; $n < strlen($index); $n++) {
                $i[] = substr($index, $n, 1);
            }

            $passhash = hash('sha256', $passKey);
            $passhash = (strlen($passhash) < strlen($index)) ? hash('sha512', $passKey) : $passhash;

            for ($n = 0; $n < strlen($index); $n++) {
                $p[] = substr($passhash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            $index = implode($i);
        }

        $base = strlen($index);

        if ($to_num) {
            // Digital number  <<--  alphabet letter code
            $in = strrev($in);
            $out = 0;
            $len = strlen($in) - 1;
            for ($t = 0; $t <= $len; $t++) {
                $bcpow = pow($base, $len - $t);
                $out = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
            }

            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $out -= pow($base, $pad_up);
                }
            }
            $out = sprintf('%F', $out);
            $out = substr($out, 0, strpos($out, '.'));
        } else {
            // Digital number  -->>  alphabet letter code
            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $in += pow($base, $pad_up);
                }
            }

            $out = "";
            for ($t = floor(log($in, $base)); $t >= 0; $t--) {
                $bcp = pow($base, $t);
                $a = floor($in / $bcp) % $base;
                $out = $out . substr($index, $a, 1);
                $in = $in - ($a * $bcp);
            }
            $out = strrev($out); // reverse
        }

        return $out;
    }

    public static function authenticate(Usuario $usuario) {
        if ($usuario->usuario_grupo_id == Usuario::GRUPO_ALUNO) {
            $login = $usuario->Aluno[0]->cpf;
            $senha = $usuario->senha;
            $Identity_column = "cpf";
        } else if ($usuario->usuario_grupo_id == Usuario::GRUPO_ADMINISTRADOR) {
            $login = $usuario->email;
            $senha = $usuario->senha;
            $Identity_column = "email";
        }

        if (!isset($login) || $login == '' || !isset($senha) || $senha == '')
            return FALSE;


        $db_config = Zend_Registry::getInstance()->get('database');
        $dbAdapter = Zend_Db::factory($db_config->db->adapter, array(
                    'host' => $db_config->db->params->host,
                    'username' => $db_config->db->params->username,
                    'password' => $db_config->db->params->password,
                    'dbname' => $db_config->db->params->dbname
        ));

        $adpter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $adpter->setTableName('usuario')
                ->setIdentityColumn($Identity_column)
                ->setCredentialColumn('senha');

        $select = $adpter->getDbSelect();
        $select->join('empresa', 'usuario.empresa_id = empresa.id', array('empresa.status' => 'status'));
        $select->joinLeft('administrador', 'usuario.id = administrador.usuario_id', array('administrador_nome' => 'nome'));
        $select->joinLeft('aluno', 'usuario.id = aluno.usuario_id', array('aluno_nome' => 'nome'));

        $select->where('usuario.usuario_grupo_id = "' . $usuario->usuario_grupo_id . '"');
        $select->where('usuario.status = "' . Usuario::ATIVO . '"');
        $select->where('empresa.status = "' . Empresa::ATIVO . '"');

        if ($usuario->usuario_grupo_id == Usuario::GRUPO_ALUNO) {
            $select->where('aluno.sistema = "' . $usuario->Aluno[0]->sistema . '"');
        }

        $adpter->setIdentity($login)
                ->setCredential($senha);

        $auth = Zend_Auth::getInstance();
        $resultado = $auth->authenticate($adpter);

        return $resultado->isValid();
    }

    public static function busca_cep($cep) {
        //$resultado = @file_get_contents('http://republicavirtual.com.br/web_cep.php?cep=' . urlencode($cep) . '&formato=query_string');

        $resultado = @file_get_contents('https://viacep.com.br/ws/' . urlencode($cep) . '/json/');
        $resultado = json_decode($resultado);

        if (!$resultado) {
            $resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";
        } else {
            $retorno['resultado'] = 1;
        }
        parse_str($resultado, $retorno);
        $retorno['resultado_txt'] = $retorno['resultado_txt'];
        $retorno['cidade'] = $resultado->localidade;
        $retorno['uf'] = $resultado->uf;
        $retorno['bairro'] = $resultado->bairro;
        $retorno['tipo_logradouro'] = '';//$resultado->bairro;
        $retorno['logradouro'] = $resultado->logradouro;
        $retorno['ibge'] = $resultado->ibge;

        return $retorno;
    }

    public static function convertArrayValuesToUtf8Decode(array $array) {
        foreach ($array as $key => $value) {
            if (is_string($value))
                $array[$key] = utf8_encode($value);
        }
        return $array;
    }

    public static function getBrowser() {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('|MSIE ([0-9].[0-9]{1,2})|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Interet Explorer ';
        } elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Opera ';
        } elseif (preg_match('|Firefox/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Firefox ';
        } elseif (preg_match('|Chrome/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Chrome ';
        } elseif (preg_match('|Safari/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Safari ';
        } elseif (preg_match('|NETSCAPE/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Netscape ';
        } else {
            // browser not recognized!
            $browser_version = 0;
            $browser = 'other';
        }
        return $browser . $browser_version;
    }

    // Obtém o input, e desfaz-se dos caracteres indesejados
    public static function gerar_link_seo($input, $substitui = '-', $remover_palavras = true, $array_palavras = array()) {
        //Colocar em minúsculas, remover a pontuação
        $input = strtolower(utf8_decode($input));
        $input = strtr($input, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
        $resultado = trim(ereg_replace(' +', ' ', preg_replace('/[^a-zA-Z0-9\s]/', '', strtolower($input))));

        //Remover as palavras que não ajudam no SEO
        //Coloco as palavras por defeito no remover_palavras(), assim eu não esse array
        if ($remover_palavras) {
            $resultado = self::remover_palavras($resultado, $substitui, $array_palavras);
        }

        //Converte os espaços para o que o utilizador quiser
        //Normalmente um hífen ou um underscore
        return str_replace(' ', $substitui, $resultado);
    }

    private static function remover_palavras($input, $substitui, $array_palavras = array(), $palavras_unicas = true) {
        //Separar todas as palavras baseadas em espaços
        $array_entrada = explode(' ', $input);

        //Criar o array de saída
        $resultado = array();

        //Faz-se um loop às palavras, remove-se as palavras indesejadas e mantém-se as que interessam
        foreach ($array_entrada as $palavra) {
            if (!in_array($palavra, $array_palavras) && ($palavras_unica ? !in_array($palavra, $resultado) : true)) {
                $resultado[] = $palavra;
            }
        }

        return implode($substitui, $resultado);
    }

    public static function validDate($data) {
        if ($data == "")
            return false;

        $data = explode("-", $data);
        return checkdate($data[1], $data[2], $data[0]);
    }

    public function validEmail($email) {
        
        if (checkdnsrr(array_pop(explode("@", $email)))) {
            return true;
        } elseif (array_pop(explode("@", $email)) == "gmail.com.br") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retorna o número da parcela utilizando a observação do boleto
     *
     * @param type $observacao
     * @return string
     */
    public static function getParcelaNumero($observacao, $quantidade_parcelas = true) {
        $observacao = trim(strip_tags($observacao));
        $observacao = str_replace(array("\n","\r"), '', $observacao);

        $pos = strripos($observacao, " - Parcela ");
        $observacao = substr($observacao, $pos + 11, strlen($observacao));

        $pos = strripos($observacao, "Referente ao curso");
        $observacao = substr($observacao, 0, $pos);

        $arr = explode(" de ", $observacao);

        $result = $arr[0];

        if ($quantidade_parcelas)
            $result .= '/' . $arr[1];

        return $result;

    }

    public static function convertDateToRemessa($data) {
        if (empty($data) || $data == '')
            return '-';

        $dataformatada = explode("-", $data);
        return $dataformatada[2] . $dataformatada[1] . ($dataformatada[0]);
    }

    public static function retiraCaracter($data, $caracter) {
        return str_replace($caracter, "", $data);
    }

     public static function tirarAcentos($string) {
        $string = preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ç)/", "/(Ç)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U c C n N"), $string);
        $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
        return $string;
    }

    public static function validaCpfBool($cpf) {
        $caracter_remove = array("(", ")", "-", " ", ".");
        $cpf = str_replace($caracter_remove, "", $cpf);

        // Verifiva se o número digitado contém todos os digitos
        //$cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
        // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
            return FALSE;
        } else {   // Calcula os números para verificar se o CPF é verdadeiro
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return FALSE;
                }
            }

            return TRUE;
        }
    }

    public static function validaData($date){
        $date_array = explode("-","$date");
        $y = $date_array[0];
        $m = $date_array[1];
        $d = $date_array[2];

        return checkdate($m,$d,$y);
    }

    public static function logMe($file_name, $msg){
        $msg = '[' . date("d-m-Y H:i:s") . '] ' . $msg;

        $fp = fopen(APPLICATION_UPLOAD_PATH . "/iugu/" . $file_name . ".txt", "a");
        $escreve = fwrite($fp, $msg);
        fclose($fp);
    }

    public static function logMeStatusMensagens($file_name, $msg){
        $msg = '[' . date("d-m-Y H:i:s") . '] ' . $msg;

        $fp = fopen(APPLICATION_UPLOAD_PATH . "/status_mensagem/" . $file_name . ".txt", "a");
        $escreve = fwrite($fp, $msg);
        fclose($fp);
    }

    public static function logMeAcessoSemPermissao($file_name, $msg){
        $msg = '[' . date("d-m-Y H:i:s") . '] ' . $msg;

        $fp = fopen(APPLICATION_UPLOAD_PATH . "/acesso-permissao/" . $file_name . ".txt", "a");
        $escreve = fwrite($fp, $msg);
        fclose($fp);
    }

    public static function notContains($needle, $haystack) {
        return !(strpos($haystack, $needle) !== false);
    }

    public static function replaceTextByArray($text, $arr) {
        foreach ($arr as $key_table => $table) {
            foreach ($table as $key_parameter => $parameter) {
                $parameter = AppUtil::replaceTextCheckType($key_parameter, $parameter);
                if ($key_parameter == "telefone") {

                    $parameter = explode(";", $parameter);

                    foreach ($parameter as $key_telefone => $value_telefone) {
                        $key_param = $key_parameter. "." . $key_telefone;
                        $text = str_replace("{{ $key_table.$key_param }}", AppUtil::convertIntToTelefone($value_telefone), $text);
                    }
                } else {
                    $text = str_replace("{{ $key_table.$key_parameter }}", $parameter, $text);
                }
            }
        }

        return $text;
    }

    public static function replaceTextCheckType($key_parameter, $parameter) {
        if ($key_parameter == "cpf")
            $parameter = AppUtil::convertIntToCPF($parameter);

        if ($key_parameter == "data_nascimento" || $key_parameter == "data")
            $parameter = AppUtil::convertDateToString($parameter);


        return $parameter;
    }

    public static function getClientIP() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function isMobileUser() {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        return (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)));

    }

    public static function convertNumberToWords($number) {

        $hyphen      = '-';
        $conjunction = ' e ';
        $separator   = ', ';
        $negative    = 'menos ';
        $decimal     = ' ponto ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'um',
            2                   => 'dois',
            3                   => 'três',
            4                   => 'quatro',
            5                   => 'cinco',
            6                   => 'seis',
            7                   => 'sete',
            8                   => 'oito',
            9                   => 'nove',
            10                  => 'dez',
            11                  => 'onze',
            12                  => 'doze',
            13                  => 'treze',
            14                  => 'quatorze',
            15                  => 'quinze',
            16                  => 'dezesseis',
            17                  => 'dezessete',
            18                  => 'dezoito',
            19                  => 'dezenove',
            20                  => 'vinte',
            30                  => 'trinta',
            40                  => 'quarenta',
            50                  => 'cinquenta',
            60                  => 'sessenta',
            70                  => 'setenta',
            80                  => 'oitenta',
            90                  => 'noventa',
            100                 => 'cento',
            200                 => 'duzentos',
            300                 => 'trezentos',
            400                 => 'quatrocentos',
            500                 => 'quinhentos',
            600                 => 'seiscentos',
            700                 => 'setecentos',
            800                 => 'oitocentos',
            900                 => 'novecentos',
            1000                => 'mil',
            1000000             => array('milhão', 'milhões'),
            1000000000          => array('bilhão', 'bilhões'),
            1000000000000       => array('trilhão', 'trilhões'),
            1000000000000000    => array('quatrilhão', 'quatrilhões'),
            1000000000000000000 => array('quinquilhão', 'quinquilhões')
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words só aceita números entre ' . PHP_INT_MAX . ' à ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . AppUtil::convertNumberToWords(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $conjunction . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = floor($number / 100)*100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds];
                if ($remainder) {
                    $string .= $conjunction . AppUtil::convertNumberToWords($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                if ($baseUnit == 1000) {
                    $string = AppUtil::convertNumberToWords($numBaseUnits) . ' ' . $dictionary[1000];
                } elseif ($numBaseUnits == 1) {
                    $string = AppUtil::convertNumberToWords($numBaseUnits) . ' ' . $dictionary[$baseUnit][0];
                } else {
                    $string = AppUtil::convertNumberToWords($numBaseUnits) . ' ' . $dictionary[$baseUnit][1];
                }
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= AppUtil::convertNumberToWords($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    public static function convertTextToBytes($val)
    {
        $val  = trim($val);

        if (is_numeric($val))
            return $val;

        $last = strtolower($val[strlen($val)-1]);
        $val  = substr($val, 0, -1); // necessary since PHP 7.1; otherwise optional

        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }

    public static function addDayInDate($days_add, $date) {
        $new_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime($date)) . "+" . $days_add . " day"));

        return $new_date;
    }

    public static function addDayInDateTime($days_add, $date) {
        $new_date = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($date)) . "+" . $days_add . " day"));

        return $new_date;
    }

    public static function addMonthsInDate($months_add, $date) {
        $new_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime($date)) . "+" . $months_add . " months"));

        return $new_date;
    }

    public static function convertMonthsInDays($months) {
        $days = round($months * 30);

        return $days;
    }

    public static function generateToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }

    public static function convertDateToBigText($city = "Belo Horizonte", $date) {
        setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );
        date_default_timezone_set( 'America/Sao_Paulo' );

        return $city . ", " . strftime( '%d de %B de %Y', strtotime( $date ) );
    }

    public static function like($needle, $haystack)
    {
        $regex = '/' . str_replace('%', '.*?', $needle) . '/';

        return preg_match($regex, $haystack) > 0;
    }

    public static function trataNumeroCelular($telefone_celular){

        //verificando se é celular
        $celReal = array ("9","8","7","6","5","4");

        // Verifica se e celular mesmo
        if (strlen($telefone_celular) == 8 || strlen($telefone_celular) == 10


        ) {

            $validaCel = substr($telefone_celular,-8,1);

            if (in_array($validaCel, $celReal)){

                // Se nao tiver DDD e 9 digito

                if (strlen($telefone_celular) == 8)
                    $telefone_celular = '9'.$telefone_celular;

                // Se tiver DDD mas nao tiver o 9 digito
                if (strlen($telefone_celular) == 10) {

                    $inicio = substr($telefone_celular, 0, 2);
                    $fim =  substr($telefone_celular, 2, 10);
                    $telefone_celular = $inicio.'9'.$fim;

                };

                return $telefone_celular;

            } else {

                $telefone_fixo = $telefone_celular;
                return $telefone_fixo;
            }
        }else{
            return $telefone_celular;
        }
    }

    public static function encryptSSL( $password ) {

        $passpharse = 'qJB0rGtIn5UB1xG03efyCp';

        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($password, $cipher, $passpharse, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $passpharse, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

        return $ciphertext;


    }

    public static function decryptSSL( $password ) {

        $retorno = '';
        $passpharse = 'qJB0rGtIn5UB1xG03efyCp';

        $c = base64_decode($password);

        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $passpharse, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $passpharse, $as_binary=true);

        if (hash_equals($hmac, $calcmac)){
            $retorno = $original_plaintext;
            return $retorno;
        }// timing attack safe comparison

        return $retorno;
    }
}
