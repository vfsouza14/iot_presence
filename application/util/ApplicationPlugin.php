<?php

class ApplicationPlugin extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $transacao = time();
        SessionUtil::setTransacao($transacao);
    }

}
