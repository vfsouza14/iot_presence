<?php

class AuthUtil {

	const SECRET_KEY = 'minha-chave-secreta';

	private $usuario_id;
	private static $instance;

    private function __construct() {
    }

    private function __clone() { }

    private function __wakeup() { }

    public static function getInstance() {
        if(self::$instance === null){
           	 self::$instance = new AuthUtil();
        }
        return self::$instance;
    }

    public function setUsuarioId($usuario_id) {
    	$this->usuario_id = $usuario_id;
    }

    public function getUsuarioId() {
    	return $this->usuario_id;
    }

	public static function authenticate($login, $senha) {
		$db_config = Zend_Registry::getInstance()->get('database');
		$dbAdapter = Zend_Db::factory($db_config->db->adapter, array(
			'host' => $db_config->db->params->host,
			'username' => $db_config->db->params->username,
			'password' => $db_config->db->params->password,
			'dbname' => $db_config->db->params->dbname,
			'driver_options' => array(
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
			)
		));

		$adpter = new Zend_Auth_Adapter_DbTable($dbAdapter);

		$adpter->setTableName('usuario')
		->setIdentityColumn('usuario.email')
		->setCredentialColumn('usuario.senha');

		$select = $adpter->getDbSelect();
		$select->join('empresa', 'usuario.empresa_id = empresa.id', array('empresa.status' => 'status'));
		$select->joinLeft('administrador', 'usuario.id = administrador.usuario_id', array('administrador_nome' => 'nome'));
		$select->joinLeft('aluno', 'usuario.id = aluno.usuario_id', array('aluno_nome' => 'nome'));

		//$select->where('usuario.usuario_grupo_id = "' . Usuario::GRUPO_ADMINISTRADOR . '"');
		$select->where('usuario.status = "' . Usuario::ATIVO . '"');
		$select->where('empresa.status = "' . Empresa::ATIVO . '"');

		$adpter->setIdentity($login)
		->setCredential($senha);

		$auth = Zend_Auth::getInstance();
		$resultado = $auth->authenticate($adpter);

		$info = $adpter->getResultRowObject(null, 'senha');

		$authenticate['info'] = $info;
		$authenticate['result'] = $resultado;

		return $authenticate;
	}

	public static function tokenVerify($token) {
		$result = FALSE;

		if ($token) {
			require_once LIBRARY_PATH . '/JWT/src/JOSE/JWT.php';
			require_once LIBRARY_PATH . '/JWT/src/JOSE/JWS.php';

			$jwt = JOSE_JWT::decode($token);
			$jws = new JOSE_JWS($jwt);
			$verify = $jws->verify(AuthUtil::SECRET_KEY, 'HS256');

			AuthUtil::getInstance()->setUsuarioId($verify->claims['sub']);

			$result = ($verify->claims['iss'] == 'siead.presence_iot.edu.br' && $verify->claims['sub'] > 0);
		}

		return $result;
	}

	public static function tokenGenerate($sub, $email, $grupo, $expires_in = 3600) {
		$result = NULL;

		require_once LIBRARY_PATH . '/JWT/src/JOSE/JWT.php';
		require_once LIBRARY_PATH . '/JWT/src/JOSE/JWS.php';

		$jwt_array = array (
			'iss' => 'siead.presence_iot.edu.br',
			'iat' => time(),
			'exp' => time() + $expires_in,
			'sub' => $sub,
			'context' => array(
				'email' => $email,
				'grupo' => $grupo
			)
		);

		$jwt = new JOSE_JWT($jwt_array);
		$jws = new JOSE_JWS($jwt);
		$jws = $jws->sign(AuthUtil::SECRET_KEY, 'HS256');

		$result = (object) array (
			'access_token' 	=> $jws->toString(),
			'token_type'	=> 'bearer',
			'expires_in'	=> $expires_in,
		);

		return $result;
	}

}
