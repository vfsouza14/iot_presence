<?php

abstract class DaoGeneric extends Doctrine_Record {

    const ATIVO = 1;
    const INATIVO = 2;
    const EXCLUIDO = 3;

    private $auditing = TRUE;

    public function save(Doctrine_Connection $conn = null) {
        $metodo = (current($this->identifier()) > 0) ? $metodo = 'update' : 'create';

        parent::save($conn);

        $this->_addAuditing($metodo);
    }

    public function delete(Doctrine_Connection $conn = null) {
        parent::delete($conn);

        $this->_addAuditing('delete');
    }

    public function setAuditing($auditing) {
        $this->auditing = $auditing;
    }

    public function setEmpresa($empresa) {
        if (Empresa::isMaster())
            $this->empresa_id = $empresa;
        else
            $this->empresa_id = Empresa::getLogged()->id;
    }

    public function isValid() {
        return true;
    }

    public function getStatus() {
        switch ($this->status) {

            case Empresa::ATIVO:
                return 'Ativo';

            case Empresa::INATIVO:
                return 'Inativo';

            case Empresa::EXCLUIDO:
                return 'Excluído';

            default:
                return 'Indefinido';
        }
    }

    private function _addAuditing($metodo) {
        if (!$this->auditing)
            return;

        $obj = $this->toArray(FALSE);

        $auditoria = new Auditoria();
        $auditoria->setAuditing(FALSE);
        $auditoria->objeto_id = current($this->identifier());
        $auditoria->objeto_classe = get_class($this);
        $auditoria->objeto = json_encode($obj);
        $auditoria->metodo = $metodo;
        $auditoria->data = AppUtil::getCurrentDate();
        $auditoria->transacao = SessionUtil::getTransacao();
        $auditoria->ip = getenv("REMOTE_ADDR");
        $auditoria->browser = AppUtil::getBrowser();

        $auditoria->empresa_id = (Empresa::getLogged()->id <= 0) ? Empresa::EMPRESA_MASTER_ID : Empresa::getLogged()->id;
        $auditoria->usuario_id = (Usuario::getLogged()->id <= 0) ? Usuario::USUARIO_MASTER_ID : Usuario::getLogged()->id;

        if(SessionUtil::getUsuarioMaster() > 0 )
            $auditoria->acessado_por = SessionUtil::getUsuarioMaster();


        $auditoria->save();
    }

    public function setTelefone($telefones) {
        $caracter_remove = array("(", ")", "-", " ", ".");

        $telefones = str_replace($caracter_remove, "", $telefones);

        $this->telefone = implode(";", $telefones);
    }

    public function getTelefone($i = 0) {
        $telefones = explode(";", $this->telefone);

        if ($i >= count($telefones))
            return "";

        return $telefones[$i];
    }

}
