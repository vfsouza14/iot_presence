<?php

class DbUtil {

    public static function setConnectionDoctrine() {
        $db_config = Zend_Registry::getInstance()->get('database');

        $dsn = 'mysql:dbname=' . $db_config->db->params->dbname . ';host=' . $db_config->db->params->host . '';
        $user = $db_config->db->params->username;
        $password = $db_config->db->params->password;
        $dbh = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8';" ));
        $conn = Doctrine_Manager::connection($dbh);
    }

    public static function generateModels() {
        Doctrine_Core::generateModelsFromDb('../application/models', array('doctrine'), array(
            'baseClassName' => 'DaoGeneric',
            'packagesPrefix' => 'Package',
            'packagesPath' => '',
            'packagesFolderName' => 'packages',
            'suffix' => '.php',
            'generateBaseClasses' => true,
            'generateTableClasses' => false,
            'generateAccessors' => false,
            'baseClassPrefix' => 'Dao',
            'baseClassesDirectory' => 'dao',)
        );
    }

}
