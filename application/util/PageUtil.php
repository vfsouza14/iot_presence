<?php

class PageUtil extends Zend_View {

    private static $_title = '';

    public static function getTitle($page = null, $titulo = null) {
        if (isset($titulo))
            return $titulo . ' - ' . PageUtil::$_title;

        if (isset($page))
            return $page . ' - ' . PageUtil::$_title;

        return PageUtil::$_title;
    }

    public static function getLogo() {
        $usuario = Zend_Auth::getInstance()->getIdentity();
        $escolaRepository = new EscolaRepository();
        $escola = $escolaRepository->getById($usuario->escola_id);

        if ($escola->id > 0) {
            return AppUtil::getFileView($escola, 'e', '60');
        } else {
            return '/content/img/logo_sbrasil.png';
        }
    }

    public static function setTitle($title) {
        PageUtil::$_title = $title;
    }

    public static function getConfig($param = null) {

        return isset($param) ? PageUtil::_getCustomVariables($param) : NULL;
    }

    private static function _setCustomVariables($param, $value) {
        $config = Zend_Registry::getInstance()->get('custom_variables');

        $config->custom_variables->page->$param = $value;

        $writer = new Zend_Config_Writer_Ini(array('config' => $config, 'filename' => APPLICATION_PATH .'/configs/custom_variables.ini'));
        $writer->write();
    }

    private static function _getCustomVariables($param) {
        $config = Zend_Registry::getInstance()->get('custom_variables');

        return $config->custom_variables->page->$param;
    }

    public static function _getDocumentosRejeitados() {
        $aluno_repository = new AlunoRepository();
        $aluno = $aluno_repository->getByUsuario(Usuario::getLogged()->id);

        $documentos_rejeitado = new AlunoDocumentoAnaliseRepository();
        $rejeitado_existe = $documentos_rejeitado->rejeitadoExiste($aluno->id, AlunoDocumentoAnalise::REJEITADO);

        return $rejeitado_existe;
    }

}
