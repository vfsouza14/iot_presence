<?php

class SessionUtil {

    public static function setTransacao($transacao) {
        $session = new Zend_Session_Namespace('application_session');
        $session->transacao = $transacao;
    }

    public static function getTransacao() {
        $session = new Zend_Session_Namespace('application_session');
        return $session->transacao;
    }

    public static function setUsuarioMaster($usuario) {
        $session = new Zend_Session_Namespace('application_session');
        $session->usuario = $usuario;

    }

    public static function getUsuarioMaster() {
        $session = new Zend_Session_Namespace('application_session');
        return $session->usuario;
    }

    public static function setReturnUrl($return_url, $data) {
        $session = new Zend_Session_Namespace('return_url_session');
        $session->return_url = $return_url;
        $session->data = $data;
    }

    public static function getReturnUrl() {
        $session = new Zend_Session_Namespace('return_url_session');

        $data['return_url'] = $session->return_url;
        $data['data'] = $session->data;

        $session->return_url = "";
        $session->data = "";

        return $data;
    }

    public static function setAccessToken($accessToken) {
        $session = new Zend_Session_Namespace('auth_session');
        $session->accessToken = $accessToken;
    }

    public static function getAccessToken() {
        $session = new Zend_Session_Namespace('auth_session');
        return $session->accessToken;
    }

    public static function destroyAccessToken() {
        Zend_Session::namespaceUnset('auth_session');
    }

    public static function configExpirationSession() {
        $tempo = self::_getTimeSession();

        $session = new Zend_Session_Namespace(Zend_Auth::getInstance()->getStorage()->getNamespace());
        $session->setExpirationSeconds($tempo);
    }

    public static function setAcessoSistema($acesso_sistema) {
        $session = new Zend_Session_Namespace('acesso_sistema');
        $session->acesso_sistema = $acesso_sistema;
        $session->acesso_multiplo = true;
    }

    public static function getAcessoSistema() {
        $session = new Zend_Session_Namespace('acesso_sistema');
        return $session;
    }

    public static function destroyAcessoSistema() {
        Zend_Session::namespaceUnset('acesso_sistema');
    }

    private static function _getTimeSession() {

//        $usuario = Zend_Auth::getInstance()->getIdentity();
//        $escolaRepository = new EscolaRepository();
//        $escola = $escolaRepository->getById($usuario->escola_id);
//        $tempo =  $escola->EscolaConfiguracao[0]->expira_sessao * 60;

        $tempo = 0;

        if ($tempo <= 0)
            $tempo = 21600;

        return $tempo;
    }

}
