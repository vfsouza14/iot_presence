<?php

class ViewUtil {

    private static $_SELECTED = "selected";
    private static $_CHECKED = "checked";
    private static $_DISABLED = "disabled";
    private static $_OK = "X";

    public static function fillFieldSelectMultiple($valores, $item) {
        return ViewUtil::fillValues($valores, $item, ViewUtil::$_SELECTED);
    }

    public static function fillFieldCheckMultiple($valores, $item) {
        return ViewUtil::fillValues($valores, $item, ViewUtil::$_CHECKED);
    }

    public static function fillFieldDisabledMultiple($valores, $item) {
        return ViewUtil::fillValues($valores, $item, ViewUtil::$_DISABLED);
    }

    public static function fillFieldCheckMultipleScreen($valores, $item) {
        return ViewUtil::fillValuesScreen($valores, $item, ViewUtil::$_OK);
    }

    public static function fillFieldSelect($valor, $item) {
        return ViewUtil::fillValue($valor, $item, ViewUtil::$_SELECTED);
    }

    public static function fillFieldCheck($valor, $item) {
        return ViewUtil::fillValue($valor, $item, ViewUtil::$_CHECKED);
    }

    private static function fillValuesScreen($valores, $item, $text_result) {
        if (isset($valores)) {
            foreach ($valores as $value) {
                if ($value == $item) {
                    return $text_result;
                }
            }
        }

        return "&nbsp;&nbsp";
    }

    private static function fillValues($valores, $item, $text_result) {
        if (isset($valores)) {
            foreach ($valores as $value) {
                if ($value == $item) {
                    return $text_result;
                }
            }
        }

        return "";
    }
    
     public static function fillValueZero($valor) {
        if (isset($valor) && $valor > 0) {
            return $valor;
        }

        return "";
    }

    private static function fillValue($valor, $item, $text_result) {
        if (isset($valor) && $valor == $item) {
            return $text_result;
        }

        return "";
    }

    public static function getOrderBy($field_title, $field, RepositoryOrder $repository_order = null) {
        if (!is_null($repository_order)) {

            $order = $repository_order->getOrderByField($field);
            $sorting_class = 'sorting';
            
            if ($repository_order->isActive($field)) {
                $caret = ($order == 'DESC') ? 'caret' : 'caret-invert';
                $caret = '<span style="margin: 7px 0px 0px 0px; float: right;" class="' . $caret . '"></span>';
                $sorting_class = ($order == 'DESC') ? 'sorting_desc' : 'sorting_asc';
            }

            //return '<th class="' . $sorting_class  . '"><a href="?sort=' . $field . '&amp;order=' . $order . $repository_order->getParamString() . '">' . $field_title . '</a></th>';
            return '<th class="' . $sorting_class  . '">' . $field_title . '</th>';
        }



        //return '<a href="?sort=' . $field . '&amp;order=DESC">' . $field_title . '</a>';
        return $field_title;
    }

    public static function decreaseText($text = "", $size = 100, $remove_tags = true) {
        if ($remove_tags)
            $text = strip_tags($text);

        $text_decrease = substr($text, 0, $size);

        if (strlen($text) > $size)
            $text_decrease .= " ...";

        return $text_decrease;
    }
    
    public static function removeTags($text = "") {
        $text = html_entity_decode($text, ENT_COMPAT | ENT_HTML401, "UTF-8");
        return strip_tags($text, '<br><br/><b><i><li><ol><ul><p>');
    }

    public static function disableLayout($layout) {
        if ($layout == 1) 
           return "layout_no_menu";
        
        return "layout";
    }
    
    public static function upperText($text) {
        return strtr(strtoupper($text), "àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿº", "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞßº");
    }

    public static function clearTagsHtmlText($text) {
        return trim(str_replace('&nbsp;', ' ', preg_replace('/\s\s+/', '', strip_tags($text))));
    }
    

}
