<?php
//https://{HOST}/deploy.php?e={ENV: dev|staging|production}&k={KEY}

$env = isset($_GET['e']) ? $_GET['e'] : null;
$key = isset($_GET['k']) ? $_GET['k'] : null;

echo 'DEPLOY INICIADO'.PHP_EOL;
echo '======='.PHP_EOL;
  
if ($env && $key == 'a2cfe19e8065517b655f1360ff4625de') {
    chdir(__DIR__.'/../scripts/' . $env);

    echo 'EXECUTANDO deploy_script.sh'.PHP_EOL;
    exec("sh deploy_script.sh 2>&1 > deploy_script.log &");
    echo '======='.PHP_EOL;

} else {
    echo 'CHAVE NÁO INFORMADA'.PHP_EOL;
    echo '======='.PHP_EOL;
}

echo 'DEPLOY CONCLUÍDO'.PHP_EOL;