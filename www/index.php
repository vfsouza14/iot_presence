<?php

//If the HTTPS is not found to be "on"
if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on") {
    //Tell the browser to redirect to the HTTPS URL.
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    //Prevent the rest of the script from executing.
    exit;
}

// Define path to application directory
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(ROOT_PATH . '/../application'));
defined('CONFIG_PATH') || define('CONFIG_PATH', realpath(ROOT_PATH . '/../configs'));
defined('LIBRARY_PATH') || define('LIBRARY_PATH', realpath(ROOT_PATH . '/../../library'));

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(realpath(LIBRARY_PATH), get_include_path(),)));

// Create application, bootstrap, and run
require_once 'Zend/Application.php';
$application = new Zend_Application(APPLICATION_ENV, CONFIG_PATH . '/application.ini');

require_once('Doctrine.php');
require_once('AppUtil.php');

spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('AppUtil', 'autoload'));

//set Multi Tenant by hostname
$multi_tenant = new Zend_Config_Ini(CONFIG_PATH . '/application.ini', $_SERVER["HTTP_HOST"]);
defined('TENANT_PATH') || define('TENANT_PATH', CONFIG_PATH  . '/' . $multi_tenant->TENANT_PATH);

// set Tenant Application Configs
$tenant = new Zend_Config_Ini(TENANT_PATH . '/tenant.ini');
$application->setOptions($tenant->get(APPLICATION_ENV)->toArray());

defined('APPLICATION_UPLOAD_PATH') || define('APPLICATION_UPLOAD_PATH', realpath($tenant->path->TENANT_UPLOAD_PATH));
defined('TENANT_PERMISSAO_PATH') || define('TENANT_PERMISSAO_PATH', realpath($tenant->path->TENANT_PERMISSAO_PATH));

if (!(APPLICATION_UPLOAD_PATH && APPLICATION_UPLOAD_PATH)) {
    echo 'Diretório do TENANT não encontrado!';die;
}
    

//registry Configs
Zend_Registry::getInstance()->set('database', $tenant->database);
Zend_Registry::getInstance()->set('files', $tenant->files);

//registry Plugins
Zend_Controller_Front::getInstance()->registerPlugin(new AclPlugin());
Zend_Controller_Front::getInstance()->registerPlugin(new ApplicationPlugin());

date_default_timezone_set('America/Sao_Paulo');
error_reporting(E_ERROR | E_PARSE /*| E_WARNING | E_NOTICE*/);

//start DB Connection
DbUtil::setConnectionDoctrine();
DbUtil::generateModels();

$application->bootstrap()->run();